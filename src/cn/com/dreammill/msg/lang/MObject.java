/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.lang
 * @File:       MObject.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2019-01-02  Conan       新建
 */
package cn.com.dreammill.msg.lang;

import cn.com.dreammill.msg.tool.MStringUtils;
import org.w3c.dom.Element;

/**
 * @Enum:       MObject
 * @Brief;      拆组包底层对象类
 */
public abstract class MObject {
    protected Element eltRoot = null;
    protected String key = "";
    protected String type = "";
    protected String explain = "";
    protected String version = "";
    protected String encoding = "";
    protected String format = "";

    public static final String KEY = "key";                                                                             // 键
    public static final String DICT = "dict";                                                                           // 字典
    public static final String TYPE = "type";                                                                           // 类型
    public static final String EXPLAIN = "explain";                                                                     // 说明
    public static final String VERSION = "version";                                                                     // 版本
    public static final String ENCODING = "encoding";                                                                   // 编码
    public static final String EQUALS = "equal";                                                                        // 存在
    public static final String FORMAT = "format";                                                                       // 格式
    public static final String SEPARATOR = "separator";                                                                 // 分隔符
    public static final String SPLIT = "split";                                                                         // 分割符
    public static final String TEXT = "text";                                                                           // 文本识别
    public static final String START = "start";                                                                         // 开始
    public static final String LENGTH = "length";                                                                       // 长度
    public static final String PATH = "path";
    public static final String MUST = "must";
    public static final String MASK = "mask";
    public static final String VALUE = "value";
    public static final String CLASS = "class";
    public static final String FUNCTION = "func";
    public static final String HEAD = "head";
    public static final String EXIST = "exist";
    public static final String SYSTEM = "system";

    /**
     * @Func:   init
     * @Brief:  初始化函数
     * @Param:  Name        Type    IO  Brief
     *          elt         Element IN  元素
     *          version     String  IN  版本
     *          encoding    String  IN  编码
     */
    protected void init(final Element elt, final String version, final String encoding) {
        this.eltRoot = elt;
        this.key = elt.getNodeName();
        this.type = (elt.hasAttribute(TYPE) ? elt.getAttribute(TYPE) : "");
        this.explain = (elt.hasAttribute(EXPLAIN) ? elt.getAttribute(EXPLAIN) : "");
        this.version = (elt.hasAttribute(VERSION) ? elt.getAttribute(VERSION) : version);
        this.encoding = (elt.hasAttribute(ENCODING) ? elt.getAttribute(ENCODING) : encoding);
    }

    /**
     * @Func:   toString
     * @Brief:  转字符串
     * @Return: 转字符串的结果，用以日志打印（String）
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("标签:(");
        sb.append(key);
        if(MStringUtils.isExist(type)) {
            sb.append(") 类型:(");
            sb.append(type);
        }
        if(MStringUtils.isExist(explain)) {
            sb.append(") 说明:(");
            sb.append(explain);
        }
        if(MStringUtils.isExist(version)) {
            sb.append(") 版本:(");
            sb.append(version);
        }
        if(MStringUtils.isExist(encoding)) {
            sb.append(") 编码:(");
            sb.append(encoding);
        }
        sb.append(")");

        return sb.toString();
    }
}
