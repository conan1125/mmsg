/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg
 * @File:       MMsg.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2019-01-03  Conan       新建
 */
package cn.com.dreammill.msg.lang;

import cn.com.dreammill.msg.MMsg;
import cn.com.dreammill.msg.element.MDict;
import cn.com.dreammill.msg.element.MElement;
import cn.com.dreammill.msg.element.MValue;
import cn.com.dreammill.msg.exception.IllicitFormatException;
import cn.com.dreammill.msg.exception.IllicitTypeException;
import cn.com.dreammill.msg.exception.IllicitValueException;
import cn.com.dreammill.msg.exception.LackDomainException;
import cn.com.dreammill.msg.field.MField;
import cn.com.dreammill.msg.field.base.MFastBase;
import cn.com.dreammill.msg.tool.MStringUtils;
import cn.com.dreammill.msg.util.MMsgInfo;
import org.apache.log4j.Logger;
import org.dom4j.DocumentException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

/**
 * @Class:      MBase
 * @Brief;      拆组包基础
 * @Extend:     MObject
 */
public class MBase extends MObject {
    private static Logger log = Logger.getLogger(MBase.class);								                            // 日志

    /**
     * @Func:   MBase
     * @Brief:  构造函数
     * @Param:  Name        Type    IO  Brief
     *          elt         Element IN  元素
     */
    public MBase(Element elt) {
        init(elt, MMsg.DEFAULT_VERSION, MMsg.DEFAULT_ENCODING);
    }

    /**
     * @Func:   identify
     * @Brief:  识别报文
     * @Param:  Name        Type    IO  Brief
     *          msg         byte[]  IN  报文
     * @Return: 报文信息(MMsgInfo)
     * @Throw:  IllicitTypeException, IllicitFormatException, UnsupportedEncodingException, IllicitValueException
     */
    public MMsgInfo identify(byte[] msg)
            throws IllicitTypeException, IllicitFormatException, UnsupportedEncodingException, IllicitValueException, DocumentException {
        NodeList nodes = eltRoot.getChildNodes();
        if(nodes != null) {
            log.trace("发现[" + nodes.getLength() + "]个节点...");
            for(int i = 0; i < nodes.getLength(); i++) {
                Node nodeChild = nodes.item(i);

                if(nodeChild.getNodeType() == Node.ELEMENT_NODE) {
                    log.debug("发现可以处理的节点[" + i + "." + nodeChild.getNodeName() + "]...");
                    encoding = (((Element) nodeChild).hasAttribute(ENCODING) ? ((Element) nodeChild).getAttribute(ENCODING) : encoding);
                    MField field = MField.create((Element)nodeChild, version, encoding);
                    if(field instanceof MFastBase) {
                        ArrayList<String> msgTps = new ArrayList<String>();
                        if(eltRoot.hasAttribute(SYSTEM)) {
                            msgTps.add(eltRoot.getAttribute(SYSTEM));
                        }

                        log.info("发现可以处理的" + field.toString());
                        return ((MFastBase) field).identify(msg, msgTps);
                    } else {
                        throw new IllicitTypeException(field.type);
                    }
                }
            }
        }

        return null;
    }

    /**
     * @Func:   unpack
     * @Brief:  解包
     * @Param:  Name        Type    IO  Brief
     *          msg         byte[]  IN  报文
     * @Return: 报文信息(MMsgInfo)
     * @Throw:  IllicitTypeException, IllicitFormatException, UnsupportedEncodingException, IllicitValueException, LackDomainException
     */
    public MDict unpack(byte[] msg)
            throws IllicitTypeException, IllicitFormatException, UnsupportedEncodingException, IllicitValueException, LackDomainException, DocumentException {
        NodeList nodes = eltRoot.getChildNodes();
        if(nodes != null) {
            log.trace("发现[" + nodes.getLength() + "]个节点...");
            for(int i = 0; i < nodes.getLength(); i++) {
                Node nodeChild = nodes.item(i);

                if(nodeChild.getNodeType() == Node.ELEMENT_NODE) {
                    log.debug("发现可以处理的节点[" + i + "." + nodeChild.getNodeName() + "]...");
                    encoding = (((Element) nodeChild).hasAttribute(ENCODING) ? ((Element) nodeChild).getAttribute(ENCODING) : encoding);

                    MField field = MField.create((Element)nodeChild, version, encoding);
                    log.info("发现可以处理的" + field.toString());
                    return field.unpack(msg);
                }
            }
        }

        return null;
    }

    /**
     * @Func:   pack
     * @Brief:  组包
     * @Param:  Name        Type    IO  Brief
     *          msg         byte[]  IN  报文
     * @Return: 报文信息(MMsgInfo)
     * @Throw:  IllicitTypeException, IllicitFormatException, IllicitValueException, IOException
     */
    public byte[] pack(MDict dict)
            throws IllicitTypeException, IllicitFormatException, IllicitValueException, IOException, DocumentException {
        NodeList nodes = eltRoot.getChildNodes();
        if(nodes != null) {
            log.trace("发现[" + nodes.getLength() + "]个节点...");
            for(int i = 0; i < nodes.getLength(); i++) {
                Node nodeChild = nodes.item(i);

                if(nodeChild.getNodeType() == Node.ELEMENT_NODE) {
                    log.debug("发现可以处理的节点[" + i + "." + nodeChild.getNodeName() + "]...");
                    encoding = (((Element) nodeChild).hasAttribute(ENCODING) ? ((Element) nodeChild).getAttribute(ENCODING) : encoding);

                    MField field = MField.create((Element)nodeChild, version, encoding);
                    log.info("发现可以处理的" + field.toString());
                    return field.pack(dict);
                }
            }
        }

        return null;
    }

    /**
     * @Func:   mapped
     * @Brief:  映射
     * @Param:  Name    Type    IO  Brief
     *          inDict  MDict   IN  原字典
     * @Return: 新字典(MDict)
     * @Throw:  IllicitTypeException, IllicitFormatException, UnsupportedEncodingException, IllicitValueException, LackDomainException
     */
    public MDict mapped(MDict inDict)
            throws IllicitTypeException, IllicitFormatException, UnsupportedEncodingException, IllicitValueException, LackDomainException {
        NodeList nodes = eltRoot.getChildNodes();
        if(nodes != null) {
            log.trace("发现[" + nodes.getLength() + "]个节点...");
            MDict outDict = new MDict();
            for(int i = 0, j = 1; i < nodes.getLength(); i++) {
                Node nodeChild = nodes.item(i);

                if(nodeChild.getNodeType() == Node.ELEMENT_NODE) {
//                    log.debug("发现可以处理的节点[" + i + "." + nodeChild.getNodeName() + "]...");
//
//                    MMapped mapped = MMapped.create((Element)nodeChild, version, encoding);
//                    log.info("第 " + (j++) + " 次发现可以处理的" + mapped.toString());
//                    try{
//                        outDict.putAll(mapped.mapped(inDict));
//                    } catch ( IllicitTypeException
//                            | LackDomainException
//                            | IllicitValueException
//                            | ClassNotFoundException
//                            | NoSuchMethodException
//                            | SecurityException
//                            | IllegalAccessException
//                            | IllegalArgumentException
//                            | InvocationTargetException
//                            | InstantiationException
//                            e) {
//                        log.error("映射失败，请关注！" + e);
//                    }
                }
            }

            return outDict;
        }
        return null;
    }

    /**
     * @Func:   check
     * @Brief:  检查
     * @Param:  Name    Type    IO  Brief
     *          dict    MDict   IN  字典
     * @Throw:  LackDomainException
     */
    public void check(MDict dict) throws LackDomainException {
        /* 获取所有子节点 */
        NodeList nodes = eltRoot.getChildNodes();

        if(nodes != null) {
            log.trace("发现[" + nodes.getLength() + "]个节点...");

            /* 遍历所有节点 */
            for(int i = 0; i < nodes.getLength(); i++) {
                Node nodeChild = nodes.item(i);

                /* 判断子节点是否是元素 */
                if(nodeChild.getNodeType() == Node.ELEMENT_NODE) {
                    log.debug("发现可以处理的节点[" + i + "." + nodeChild.getNodeName() + "]...");

                    String dictName = (((Element) nodeChild).hasAttribute(DICT) ? ((Element) nodeChild).getAttribute(DICT) : "");
                    boolean exist = (((Element) nodeChild).hasAttribute(EXIST) ? Boolean.valueOf(((Element) nodeChild).getAttribute(EXIST)) : false);

                    /* 判断字典是否存在 */
                    if(dict.containsKey(dictName)) {
                        log.debug("发现[" + dictName + "]");
                        MElement elt = dict.get(dictName);

                        if(elt instanceof MValue) {
                            if(!MStringUtils.isExist(((MValue) elt).getString())) {
                                log.error("[" + dictName + "]值为空");
                                throw new LackDomainException(dictName);
                            }
                        }
                    } else {
                        if(exist) {
                            log.error("未发现[" + dictName + "]");
                            throw new LackDomainException(dictName);
                        } else {
                            log.warn("未发现[" + dictName + "]");
                        }
                    }
                }
            }
        }
    }
}
