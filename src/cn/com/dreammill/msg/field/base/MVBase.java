/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.field.base
 * @File:       MVBase.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2019-01-03  Conan       新建
 */
package cn.com.dreammill.msg.field.base;

import cn.com.dreammill.msg.field.MField;
import org.w3c.dom.Element;


/**
 * @Class:  MMsg
 * @Brief;  拆组包
 * @Extend: MField
 */
public abstract class MVBase extends MField {
    /**
     * @Func:   MVBase
     * @Brief:  构造函数
     * @Param:  Name        Type        IO  Brief
     *          cfg         Element     IN  元素
     *          version     String      IN  版本
     *          encoding    String      IN  编码
     */
    public MVBase(Element elt, String version, String encoding) {
        super(elt, version, encoding);
    }
}
