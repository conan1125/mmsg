/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.field.base
 * @File:       MVBase.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2019-01-03  Conan       新建
 */
package cn.com.dreammill.msg.field.base;

import cn.com.dreammill.msg.exception.IllicitTypeException;
import cn.com.dreammill.msg.exception.IllicitValueException;
import cn.com.dreammill.msg.util.MMsgInfo;
import org.dom4j.DocumentException;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * @Class:  MMsg
 * @Brief;  拆组包
 * @Extend: MField
 */
public interface MFastBase {
    /**
     * @Func:   identify
     * @Brief:  识别
     * @Param:  Name    Type       IO  Brief
     *          msg     byte[]     IN  报文
     *          values  ArrayList  IN  值
     * @Return: 报文信息(MMsgInfo)
     */
    public abstract MMsgInfo identify(byte[] msg, ArrayList<String> values)
            throws UnsupportedEncodingException
            , IllicitValueException
            , IllicitTypeException
            , DocumentException
    ;
}
