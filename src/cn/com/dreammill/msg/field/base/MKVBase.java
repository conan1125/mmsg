/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.field.base
 * @File:       MKVBase.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2019-01-03  Conan       新建
 */
package cn.com.dreammill.msg.field.base;

import cn.com.dreammill.msg.MMsg;
import cn.com.dreammill.msg.element.MArray;
import cn.com.dreammill.msg.element.MDict;
import cn.com.dreammill.msg.element.MElement;
import cn.com.dreammill.msg.element.MValue;
import cn.com.dreammill.msg.exception.IllicitFormatException;
import cn.com.dreammill.msg.exception.IllicitTypeException;
import cn.com.dreammill.msg.exception.IllicitValueException;
import cn.com.dreammill.msg.exception.LackDomainException;
import cn.com.dreammill.msg.field.MField;
import cn.com.dreammill.msg.tool.MStringUtils;
import cn.com.dreammill.msg.util.MMsgInfo;
import cn.com.dreammill.msg.util.type.MMsgCfgs;
import org.apache.log4j.Logger;
import org.dom4j.DocumentException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @Class:  MKVBase
 * @Brief;  拆组包
 * @Extend: MField
 */
public abstract class MKVBase extends MField {
    private static Logger log = Logger.getLogger(MKVBase.class);

    /**
     * @Func:   MKVBase
     * @Brief:  构造函数
     * @Param:  Name        Type        IO  Brief
     *          cfg         Element     IN  元素
     *          version     String      IN  版本
     *          encoding    String      IN  编码
     */
    public MKVBase(Element elt, String version, String encoding) {
        super(elt, version, encoding);
    }

    /**
     * @Func:   identifyByKV
     * @Brief:  识别报文
     * @Param:  Name    Type        IO  Brief
     *          map     Map         IN  MAP
     *          elt     Element     IN  元素
     *          values  ArrayList   IN  值
     * @Return: 识别到的报文信息(MMsgInfo)
     *          NULL-未识别报文
     */
    @SuppressWarnings({ "unchecked", "null" })
    protected MMsgInfo identifyByKV(final Map<String, Object> map, final Element elt, final ArrayList<String> values)
            throws IllicitValueException
            , IllicitTypeException {
        String msgType = spell(values);

        log.debug("报文类型：" + msgType);
        /* 获取所有子节点 */
        NodeList nodes = elt.getChildNodes();
        if(nodes != null) {
            log.trace("发现[" + nodes.getLength() + "]个节点...");
            /* 遍历所有节点 */
            for(int i = 0; i < nodes.getLength(); i++) {
                Node nodeChild = nodes.item(i);

                /* 判断子节点是否是元素 */
                if(nodeChild.getNodeType() == Node.ELEMENT_NODE) {
                    log.debug("发现可以处理的节点[" + i + "." + nodeChild.getNodeName() + "]...");

                    /* 判断元素类型 */
                    switch(nodeChild.getNodeName()) {
                        case VALUE:
                            String keys = (
                                    ((Element) nodeChild).hasAttribute(KEY) ?
                                    ((Element) nodeChild).getAttribute(KEY) : ""
                            );
                            String format = (
                                    ((Element) nodeChild).hasAttribute(FORMAT) ?
                                    ((Element) nodeChild).getAttribute(FORMAT) : null
                            );

                            Map<String, Object> tmpMap = map;
                            if(MStringUtils.isExist(keys)) {
                                for(String key : keys.split(MMsg.DICT_SPLIT)) {
                                    Object obj = tmpMap.get(key);
                                    if(obj == null) {
                                        throw new IllicitValueException(obj.toString());
                                    } else if(obj instanceof Map) {
                                        tmpMap = (Map<String, Object>) obj;
                                    } else if(obj instanceof List) {
                                        throw new IllicitValueException(obj.toString());
                                    } else {
                                        MValue val = new MValue((String) obj, format, encoding);
                                        values.add(val.toString());
                                    }
                                }

                                return identifyByKV(map, (Element)nodeChild, values);
                            } else {
                                throw new IllicitTypeException(keys);
                            }
                        case FIELD:
                            break;
                        default:
                            if('0' <= msgType.charAt(1)
                                    && msgType.charAt(1) <= '9') {
                                msgType = "_MSG" + msgType;
                            }
                            if(msgType.equalsIgnoreCase("_" + nodeChild.getNodeName() + "_")) {
                                log.info("识别到报文类型[" + nodeChild.getNodeName() + "]...");

                                String explain = ((Element) nodeChild).getAttribute(EXPLAIN);
                                MMsgCfgs msgCfgs = getMsgCFGs((Element) nodeChild);

                                return new MMsgInfo(nodeChild.getNodeName(), explain, msgCfgs);
                            }
                            break;
                    }
                }
            }
        }

        return null;
    }

    /**
     * @Func:   unpackByKV
     * @Brief:  拆包
     * @Param:  Name    Type        IO  Brief
     *          map     Map         IN  MAP
     *          elt     Element     IN  元素
     * @Return: 拆包后字典(MDict)
     */
    @SuppressWarnings("unchecked")
    protected MDict unpackByKV(Map<String, Object> map, Element elt)
            throws UnsupportedEncodingException
            , IllicitTypeException
            , IllicitValueException
            , LackDomainException
            , IllicitFormatException
            , DocumentException {
        MDict dict = new MDict();

        /* 获取所有子节点 */
        NodeList nodes = elt.getChildNodes();
        if(nodes != null) {
            log.trace("发现[" + nodes.getLength() + "]个节点...");
            /* 遍历所有节点 */
            for(int i = 0; i < nodes.getLength(); i++) {
                Node nodeChild = nodes.item(i);

                /* 判断子节点是否是元素 */
                if(nodeChild.getNodeType() == Node.ELEMENT_NODE) {
                    log.debug("发现可以处理的节点[" + i + "." + nodeChild.getNodeName() + "]...");
                    String keyName = (((Element) nodeChild).hasAttribute(KEY) ? ((Element) nodeChild).getAttribute(KEY) : "");
                    String dictName = (((Element) nodeChild).hasAttribute(DICT) ? ((Element) nodeChild).getAttribute(DICT) : "");
                    String format = (((Element) nodeChild).hasAttribute(FORMAT) ? ((Element) nodeChild).getAttribute(FORMAT) : null);
                    boolean mask = (((Element) nodeChild).hasAttribute(MASK) ? Boolean.valueOf(((Element) nodeChild).getAttribute(MASK)) : false);

                    /* 判断元素类型 */
                    switch(nodeChild.getNodeName()) {
                        case VALUE:
                            if(map.containsKey(keyName)) {
                                log.debug("发现[" + keyName + "]");
                                Object value_tmp = map.get(keyName);
                                if(value_tmp instanceof ArrayList )
                                {
                                    log.debug("@@@@@@"+value_tmp.toString());
                                    MArray array = new MArray();
                                    for( i= 0 ; i < ((ArrayList)value_tmp).size() ; i++ )
                                    {
                                        Object o = ((ArrayList) value_tmp).get(i);
                                        MValue value = new MValue(o.toString().getBytes(encoding), format, encoding);
                                        array.add(value);
                                    }
                                    dict.put(dictName, array);
                                }else{
                                    if( value_tmp instanceof Integer )
                                    {
                                        map.put(keyName, Integer.toString( (Integer)value_tmp ) );
                                    }else if(value_tmp instanceof Long )
                                    {
                                        map.put(keyName, Long.toString( (Long)value_tmp ) );
//                                    }else if( value_tmp instanceof Double )
//                                    {
//                                        map.put(keyName, MCalcul.toString((Double)value_tmp, 2, (byte) 1) );
//                                    }else if( value_tmp instanceof Float )
//                                    {
//                                        double Float_tmp = (Float)value_tmp;
//                                        map.put(keyName, MCalcul.toString(Float_tmp, 2, (byte) 1) );
                                    }


                                    MValue value = new MValue(((String) map.get(keyName)).getBytes(encoding), format, encoding, mask);
                                    dict.put(dictName, value);
                                }
                            } else {
                                log.warn("未发现[" + keyName + "]");
                            }
                            break;
                        case OBJECT:
                            if(map.containsKey(keyName)) {
                                log.debug("发现[" + keyName + "]");
                                MDict tmpDict = unpackByKV((Map<String, Object>) map.get(keyName), (Element)nodeChild);

                                if(MStringUtils.isExist(dictName)) {
                                    dict.put(dictName, tmpDict);
                                } else {
                                    dict.putAll(tmpDict);
                                }
                            } else {
                                log.warn("未发现[" + keyName + "]");
                            }
                            break;
                        case ARRAY:
                            if(map.containsKey(keyName)) {

                                log.info("发现[" + keyName + "]");
                                Object value = map.get(keyName);
                                if( value instanceof Map )
                                {
                                    ArrayList<Object> tmp = new ArrayList();
                                    tmp.add(value);
                                    map.put(keyName, tmp);
                                }
                                MArray tmpArray = unpackByKV((ArrayList<Object>) map.get(keyName), (Element)nodeChild);
                                dict.put(dictName, tmpArray);
                            } else {
                                log.info("未发现[" + keyName + "]");
                            }

                            break;
                        case FIELD:
                            if(map.containsKey(keyName)) {

                                MField field = MField.create((Element)nodeChild, version, encoding);
                                log.info("发现可以处理的" + field.toString());
                                MDict tmpDict = field.unpack(map.get(keyName).toString().getBytes());

                                if(MStringUtils.isExist(dictName)) {
                                    dict.put(dictName, tmpDict);
                                } else {
                                    dict.putAll(tmpDict);
                                }
                            }

                            break;
                        default:
                            break;
                    }
                }
            }
        }

        return dict;
    }

    /**
     * @Func:   unpackByKV
     * @Brief:  拆包
     * @Param:  Name    Type        IO  Brief
     *          map     Map         IN  MAP
     *          elt     Element     IN  元素
     * @Return: 拆包后数组(MArray)
     */
    @SuppressWarnings("unchecked")
    protected MArray unpackByKV(ArrayList<Object> map, Element elt)
            throws UnsupportedEncodingException
            , IllicitTypeException
            , IllicitValueException
            , LackDomainException
            , IllicitFormatException
            , DocumentException {
        MArray array = new MArray();

        for(Object o : map) {
            if(o instanceof Map) {
                MDict dict = unpackByKV((Map<String, Object>) o, elt);
                array.add(dict);
            } else {
                MValue value = new MValue(o.toString().getBytes(encoding), format, encoding);
                array.add(value);
            }
        }

        return array;
    }

    /**
     * @Func:   packByKV
     * @Brief:  组包
     * @Param:  Name    Type        IO  Brief
     *          dict    MDict       IN  字典
     *          elt     Element     IN  元素
     * @Return: 组包后MAP(MArray)
     */
    protected Map<String, Object> packByKV(MDict dict, Element elt) {
        Map<String, Object> map = new LinkedHashMap<String, Object>();

        /* 获取所有子节点 */
        NodeList nodes = elt.getChildNodes();
        if(nodes != null) {
            log.trace("发现[" + nodes.getLength() + "]个节点...");
            /* 遍历所有节点 */
            for(int i = 0; i < nodes.getLength(); i++) {
                Node nodeChild = nodes.item(i);

                /* 判断子节点是否是元素 */
                if(nodeChild.getNodeType() == Node.ELEMENT_NODE) {
                    log.debug("发现可以处理的节点[" + String.valueOf(i) + "." + nodeChild.getNodeName() + "]...");

                    String dictName = (
                            ((Element) nodeChild).hasAttribute(DICT) ?
                                    ((Element) nodeChild).getAttribute(DICT) : ""
                    );
                    String keyName = (
                            ((Element) nodeChild).hasAttribute(KEY) ?
                                    ((Element) nodeChild).getAttribute(KEY) : ""
                    );
                    boolean exist = (
                            ((Element) nodeChild).hasAttribute(EXIST) ?
                                    Boolean.valueOf(((Element) nodeChild).getAttribute(EXIST)) : false
                    );

                    /* 判断元素类型 */
                    switch(nodeChild.getNodeName()) {
                        case VALUE:
                            if(dict != null
                                    && dict.containsKey(dictName)) {
                                log.debug("发现[" + dictName + "]");
                                MValue value = (MValue) dict.get(dictName);
                                map.put(keyName, value.getString());
                            } else {
                                log.warn("未发现[" + dictName + "]");
                                if(exist) {
                                    map.put(keyName, "");
                                }
                            }
                            break;
                        case OBJECT:
                            if(dict != null
                                    && dict.containsKey(dictName)) {
                                log.debug("发现[" + dictName + "]");
                                Map<String, Object> tmpMap = packByKV((MDict) dict.get(dictName), (Element)nodeChild);
                                map.put(keyName, tmpMap);
                            } else {
                                log.warn("未发现[" + dictName + "]");
                                if(exist) {
                                    Map<String, Object> tmpMap = packByKV((MDict) dict.get(dictName), (Element)nodeChild);
                                    if(tmpMap != null) {
                                        map.put(keyName, tmpMap);
                                    } else {
                                        map.put(keyName, "");
                                    }
                                }
                            }
                            break;
                        case ARRAY:
                            if(dict != null
                                    && dict.containsKey(dictName)) {
                                log.debug("发现[" + dictName + "]");
                                ArrayList<Map<String, Object>> arrayMap = new ArrayList<Map<String, Object>>();
                                MArray array = (MArray) dict.get(dictName);
                                for(MElement elet: array) {
                                    Map<String, Object> tmpMap = packByKV((MDict) elet, (Element)nodeChild);
                                    arrayMap.add(tmpMap);
                                }
                                map.put(keyName, arrayMap);
                            } else {
                                log.warn("未发现[" + dictName + "]");
                                if(exist) {
                                    map.put(keyName, new ArrayList<Object>());
                                }
                            }

                            break;
                        case FIELD:
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        return map;
    }
}

