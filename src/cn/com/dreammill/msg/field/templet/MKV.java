/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.field.templet
 * @File:       MKV.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2019-01-07  Conan       新建
 */
package cn.com.dreammill.msg.field.templet;

import cn.com.dreammill.msg.element.MDict;
import cn.com.dreammill.msg.exception.IllicitFormatException;
import cn.com.dreammill.msg.exception.IllicitTypeException;
import cn.com.dreammill.msg.exception.IllicitValueException;
import cn.com.dreammill.msg.exception.LackDomainException;
import cn.com.dreammill.msg.field.base.MFastBase;
import cn.com.dreammill.msg.field.base.MKVBase;
import cn.com.dreammill.msg.tool.MStringUtils;
import cn.com.dreammill.msg.util.MMsgInfo;
import org.apache.log4j.Logger;
import org.dom4j.DocumentException;
import org.w3c.dom.Element;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @Class:  MKV
 * @Brief;  键值对
 * @Extend: MVBase
 * @Impl:   MFastBase
 */
public class MKV extends MKVBase implements MFastBase {
    private static Logger log = Logger.getLogger(MKV.class);
    private String separate = "";
    private String split = "";
    private String text = "";

    /**
     * @Func:   MKV
     * @Brief:  构造函数
     * @Param:  Name        Type        IO  Brief
     *          elt         Element     IN  元素
     *          version     String      IN  版本
     *          encoding    String      IN  编码
     *          separate    String      IN  分隔符
     *          split       String      IN  分割
     *          text        String      IN  文本标识符
     */
    public MKV(Element elt, String version, String encoding, String separate, String split, String text) {
        super(elt, version, encoding);
        this.separate = separate;
        this.split = split;
        this.text = text;
    }

    /**
     * @Func:   identify
     * @Brief:  识别报文
     * @Param:  Name    Type        IO  Brief
     *          msg     byte[]      IN  报文
     *          values  ArrayList   IN  值
     * @Return: 报文信息(MMsgInfo)
     */
    @Override
    public MMsgInfo identify(byte[] msg, ArrayList<String> values)
            throws UnsupportedEncodingException
            , IllicitValueException
            , IllicitTypeException {
        /* 解析KV报文 */
        log.trace("解析KV报文...");
        Map<String, Object> map = pareseKV2Map(new String(msg, encoding));
        log.trace("MAP=" + map);
        /* 报文识别 */
        return identifyByKV(map, eltRoot, values);
    }

    /**
     * @Func:   pack
     * @Brief:  组包
     * @Param:  Name    Type   IO  Brief
     *          dict    MDict  IN  字典
     * @Return: 报文(byte[])
     */
    @Override
    public byte[] pack(MDict dict)
            throws IOException {
        /* 生成KV报文 */
        log.trace("生成KV报文...");
        Map<String, Object> map = packByKV(dict, eltRoot);
        log.trace("MAP=" + map);
        /* 报文组包 */
        return pareseMap2KV(map).getBytes(encoding);
    }

    /**
     * @Func:   unpack
     * @Brief:  拆包
     * @Param:  Name    Type       IO  Brief
     *          msg     byte[]     IN  报文
     * @Return: 字典(MDict)
     */
    @Override
    public MDict unpack(byte[] msg)
            throws UnsupportedEncodingException
            , IllicitValueException
            , IllicitTypeException
            , LackDomainException
            , IllicitFormatException
            , DocumentException {
        /* 解析JSON报文 */
        log.trace("解析KV报文...");
        Map<String, Object> map = pareseKV2Map(new String(msg, encoding));
        log.trace("MAP=" + map);
        /* 报文拆包 */
        return unpackByKV(map, eltRoot);
    }

    /**
     * @Func:   pareseKV2Map
     * @Brief:  解析键值对报文到MAP
     * @Param:  Name    Type       IO  Brief
     *          msg     String     IN  报文
     * @Return: 字典(MDict)
     */
    private Map<String, Object> pareseKV2Map(String msg) {
        Map<String, Object> map = new HashMap<String, Object>();

        ArrayList<String> listKeyVal = new ArrayList<String>();

        /* 按照连接符拆分所有的KV对 */
        String[] kvs = msg.split(separate, -1);
        String tmpKeyVal = "";
        for(String kv : kvs) {
            tmpKeyVal += kv;
            /* 配置了文本识别域，判断文本识别是否完整
             *	不完整则取下一个KV对
             *	完整则放入KV队列中
             */
            if(MStringUtils.isExist(text)
                    && MStringUtils.appearNumber(tmpKeyVal, text) % 2 != 0) {
                continue;
            } else {
                listKeyVal.add(tmpKeyVal);
                tmpKeyVal = "";
            }
        }

        /* 按照分隔符拆分Key和Value，并存放到MAP中 */
        for(String keyVal : listKeyVal) {
            String[] tmps = keyVal.split(split, -1);
            int n = 0;
            String key = "";
            String value = "";

            for(; n < tmps.length - 1; n++) {
                key += tmps[n];
                if(MStringUtils.isExist(text)
                        && MStringUtils.appearNumber(key, text) % 2 != 0) {
                    continue;
                } else {
                    key = key.replaceAll(text, "");
                }
            }

            for(; n < tmps.length; n++) {
                value += tmps[n];
                if(MStringUtils.isExist(text)
                        && MStringUtils.appearNumber(value, text) % 2 != 0) {
                    continue;
                } else {
                    value = value.replaceAll(text, "");
                }
            }

            map.put(key, value);
        }

        return map;
    }

    /**
     * @Func:   pareseMap2KV
     * @Brief:  解析MAP到键值对报文
     * @Param:  Name    Type       IO  Brief
     *          msg     String     IN  报文
     * @Return: 报文(String)
     */
    private String pareseMap2KV(Map<String, Object> map) {
        StringBuffer sb = new StringBuffer();

        Iterator iter = map.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();

            /* 连接符 */
            if(MStringUtils.isExist(separate)
                    && sb.length() >= 0) {
                sb.append(separate);
            }
            /* 键 */
            if(MStringUtils.isExist(entry.getKey().toString())) {
                sb.append(entry.getKey().toString());
            }
            /* 赋值符 */
            if(MStringUtils.isExist(split)) {
                sb.append(split);
            }
            /* 文本标识符 */
            if(MStringUtils.isExist(text)) {
                sb.append(text);
            }
            /* 值 */
            if(MStringUtils.isExist(entry.getValue().toString())) {
                sb.append(entry.getValue().toString());
            }
            /* 文本标识符 */
            if(MStringUtils.isExist(text)) {
                sb.append(text);
            }
        }

        return sb.toString();
    }
}
