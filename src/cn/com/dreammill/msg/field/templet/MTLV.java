/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.field.templet
 * @File:       MTLV.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2019-01-07  Conan       新建
 */
package cn.com.dreammill.msg.field.templet;

import cn.com.dreammill.msg.element.MDict;
import cn.com.dreammill.msg.field.base.MKVBase;
import org.w3c.dom.Element;

/**
 * @Class:  MTLV
 * @Brief;  键值对
 * @Extend: MKVBase
 */
public class MTLV extends MKVBase {
    /**
     * @Func:   MTLV
     * @Brief:  构造函数
     * @Param:  Name        Type        IO  Brief
     *          elt         Element     IN  元素
     *          version     String      IN  版本
     *          encoding    String      IN  编码
     */
    public MTLV(Element elt, String version, String encoding) {
        super(elt, version, encoding);
    }

    /**
     * @Func:   pack
     * @Brief:  组包
     * @Param:  Name    Type   IO  Brief
     *          dict    MDict  IN  字典
     * @Return: 报文(byte[])
     */
    @Override
    public byte[] pack(MDict dict) {
        return null;
    }

    /**
     * @Func:   unpack
     * @Brief:  拆包
     * @Param:  Name    Type       IO  Brief
     *          msg     byte[]     IN  报文
     * @Return: 字典(MDict)
     */
    @Override
    public MDict unpack(byte[] msg) {
        return null;
    }
}