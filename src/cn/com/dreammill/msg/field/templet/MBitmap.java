/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.field.templet
 * @File:       MBitmap.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2019-01-03  Conan       新建
 */
package cn.com.dreammill.msg.field.templet;

import cn.com.dreammill.msg.element.MDict;
import cn.com.dreammill.msg.field.base.MVBase;
import org.w3c.dom.Element;

/**
 * @Class:  MBitmap
 * @Brief;  位图域
 * @Extend: MVBase
 */
public class MBitmap extends MVBase {
    /**
     * @Func:   MJSON
     * @Brief:  构造函数
     * @Param:  Name        Type        IO  Brief
     *          elt         Element     IN  元素
     *          version     String      IN  版本
     *          encoding    String      IN  编码
     */
    public MBitmap(Element elt, String version, String encoding) {
        super(elt, version, encoding);
    }

    /**
     * @Func:   pack
     * @Brief:  组包
     * @Param:  Name    Type   IO  Brief
     *          dict    MDict  IN  字典
     * @Return: 报文(byte[])
     */
    @Override
    public byte[] pack(MDict dict) {
        // TODO 添加位图域组包代码
        return null;
    }

    /**
     * @Func:   unpack
     * @Brief:  拆包
     * @Param:  Name    Type       IO  Brief
     *          msg     byte[]     IN  报文
     * @Return: 字典(MDict)
     */
    @Override
    public MDict unpack(byte[] msg) {
        // TODO 添加位图域拆包代码
        return null;
    }

}