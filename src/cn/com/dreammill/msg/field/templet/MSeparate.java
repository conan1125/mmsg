/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.field.templet
 * @File:       MSeparate.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2019-01-07  Conan       新建
 */
package cn.com.dreammill.msg.field.templet;

import cn.com.dreammill.msg.element.MDict;
import cn.com.dreammill.msg.element.MValue;
import cn.com.dreammill.msg.exception.IllicitFormatException;
import cn.com.dreammill.msg.exception.IllicitTypeException;
import cn.com.dreammill.msg.exception.IllicitValueException;
import cn.com.dreammill.msg.exception.LackDomainException;
import cn.com.dreammill.msg.field.MField;
import cn.com.dreammill.msg.field.base.MFastBase;
import cn.com.dreammill.msg.field.base.MVBase;
import cn.com.dreammill.msg.tool.MStringUtils;
import cn.com.dreammill.msg.util.MMsgInfo;
import cn.com.dreammill.msg.util.type.MMsgCfgs;
import org.apache.log4j.Logger;
import org.dom4j.DocumentException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * @Class:  MSeparate
 * @Brief;  分隔符域
 * @Extend: MVBase
 * @Impl:   MFastBase
 */
public class MSeparate extends MVBase implements MFastBase {
    private static Logger log = Logger.getLogger(MSeparate.class);
    private String separate = " ";

    /**
     * @Func:   MSeparate
     * @Brief:  构造函数
     * @Param:  Name        Type        IO  Brief
     *          elt         Element     IN  元素
     *          version     String      IN  版本
     *          encoding    String      IN  编码
     *          separate    String      IN  分隔符
     *          format      String      IN  格式
     */
    public MSeparate(Element elt, String version, String encoding, String separate, String format) {
        super(elt, version, encoding);
        this.separate = separate;
        this.format = format;
    }

    /**
     * @Func:   identify
     * @Brief:  识别报文
     * @Param:  Name    Type        IO  Brief
     *          msg     byte[]      IN  报文
     *          values  ArrayList   IN  值
     * @Return: 报文信息(MMsgInfo)
     */
    @Override
    public MMsgInfo identify(byte[] msg, ArrayList<String> values) {
        ArrayList<byte[]> msgList = split(msg, separate.getBytes());
        return identifyBySep(msgList, eltRoot, values);
    }

    /**
     * @Func:   pack
     * @Brief:  组包
     * @Param:  Name    Type   IO  Brief
     *          dict    MDict  IN  字典
     * @Return: 报文(byte[])
     */
    @Override
    public byte[] pack(MDict dict)
            throws IllicitValueException
            , IllicitTypeException
            , IOException
            , IllicitFormatException
            , DocumentException {
        ByteBuffer bf = packBySep(dict, eltRoot);
        bf.flip();

        byte[] msg = new byte[bf.limit()];
        bf.get(msg);

        return msg;
    }

    /**
     * @Func:   unpack
     * @Brief:  拆包
     * @Param:  Name    Type       IO  Brief
     *          msg     byte[]     IN  报文
     * @Return: 字典(MDict)
     */
    @Override
    public MDict unpack(byte[] msg)
            throws UnsupportedEncodingException
            , IllicitValueException
            , IllicitTypeException
            , LackDomainException
            , IllicitFormatException
            , DocumentException {
        ArrayList<byte[]> msgList = split(msg, separate.getBytes());
        return unpackBySep(msgList, eltRoot);
    }

    /**
     * @Func:   identifyBySep
     * @Brief:  识别报文
     * @Param:  Name    Type        IO  Brief
     *          msgs    ArrayList   IN  报文
     *          elt     Element     IN  元素
     *          values  ArrayList   IN  值
     * @Return: 报文信息(MMsgInfo)
     */
    protected MMsgInfo identifyBySep(ArrayList<byte[]> msgs, Element elt, ArrayList<String> values) {
        String msgType = spell(values);

        log.debug("报文类型：" + msgType);
        /* 获取所有子节点 */
        NodeList nodes = elt.getChildNodes();
        if (nodes != null) {
            log.trace("发现[" + nodes.getLength() + "]个节点...");
            /* 遍历所有节点 */
            for (int i = 0; i < nodes.getLength(); i++) {
                Node nodeChild = nodes.item(i);

                /* 判断子节点是否是元素 */
                if (nodeChild.getNodeType() == Node.ELEMENT_NODE) {
                    log.debug("发现可以处理的节点[" + i + "." + nodeChild.getNodeName() + "]...");

                    /* 判断元素类型 */
                    switch (nodeChild.getNodeName()) {
                        case VALUE:
                            String startVal = (
                                    ((Element) nodeChild).hasAttribute(START) ?
                                            ((Element) nodeChild).getAttribute(START) : null
                            );
                            int start = (
                                    MStringUtils.isExist(startVal) ?
                                            Integer.parseInt(startVal) : msgs.size()
                            );

                            if (msgs.size() > start) {
                                values.add(new String(msgs.get(start)));
                            }

                            return identifyBySep(msgs, (Element) nodeChild, values);
                        case FIELD:
                            break;
                        default:
                            if ('0' <= msgType.charAt(1)
                                    && msgType.charAt(1) <= '9') {
                                msgType = "_MSG" + msgType;
                            }
                            if (msgType.equalsIgnoreCase("_" + nodeChild.getNodeName() + "_")) {
                                log.info("识别到报文类型[" + msgType + "]...");

                                String explain = ((Element) nodeChild).getAttribute(EXPLAIN);
                                MMsgCfgs msgCfgs = getMsgCFGs((Element) nodeChild);

                                return new MMsgInfo(nodeChild.getNodeName(), explain, msgCfgs);
                            }
                            break;
                    }
                }
            }
        }

        return null;
    }


    /**
     * @Func:   unpackBySep
     * @Brief:  拆包
     * @Param:  Name    Type       IO  Brief
     *          msg     ArrayList  IN  报文
     *          elt     Element    IN  元素
     * @Return: 字典(MDict)
     */
    protected MDict unpackBySep(ArrayList<byte[]> msgs, Element elt)
            throws IllicitTypeException
            , IllicitValueException
            , IllicitFormatException
            , UnsupportedEncodingException
            , LackDomainException
            , DocumentException {
        MDict dict = new MDict();

        /* 获取所有子节点 */
        NodeList nodes = elt.getChildNodes();
        if (nodes != null) {
            log.trace("发现[" + nodes.getLength() + "]个节点...");
            /* 遍历所有节点 */
            for (int i = 0, n = 0; i < nodes.getLength(); i++) {
                Node nodeChild = nodes.item(i);

                /* 判断子节点是否是元素 */
                if (nodeChild.getNodeType() == Node.ELEMENT_NODE) {
                    log.debug("发现可以处理的节点[" + i + "." + nodeChild.getNodeName() + "]...");
                    String dictName = (((Element) nodeChild).hasAttribute(DICT) ? ((Element) nodeChild).getAttribute(DICT) : "");
                    String format = (((Element) nodeChild).hasAttribute(FORMAT) ? ((Element) nodeChild).getAttribute(FORMAT) : null);
                    boolean mask = (((Element) nodeChild).hasAttribute(MASK) ? Boolean.valueOf(((Element) nodeChild).getAttribute(MASK)) : false);

                    /* 判断元素类型 */
                    switch (nodeChild.getNodeName()) {
                        case VALUE:
                            byte[] tmp = msgs.get(n);
                            MValue value = new MValue(tmp, format, encoding, mask);
                            dict.put(dictName, value);
                            break;
                        case FIELD:
                            byte[] newMsg = msgs.get(n);

                            MField field = MField.create((Element) nodeChild, version, encoding);
                            log.info("发现可以处理的" + field.toString());
                            MDict tmpDict = field.unpack(newMsg);

                            if (MStringUtils.isExist(dictName)) {
                                dict.put(dictName, tmpDict);
                            } else {
                                dict.putAll(tmpDict);
                            }

                            break;
                        default:
                            break;
                    }

                    n++;
                }
            }
        }

        return dict;
    }

    /**
     * @Func:   packBySep
     * @Brief:  组包
     * @Param:  Name    Type   IO  Brief
     *          dict    MDict  IN  字典
     *          elt     Element     IN  元素
     * @Return: 报文(ByteBuffer)
     */
    protected ByteBuffer packBySep(MDict dict, Element elt)
            throws IllicitFormatException
            , IllicitTypeException
            , IllicitValueException
            , IOException
            , DocumentException {
        ByteBuffer msg = ByteBuffer.allocate(4 * 1024);

        /* 获取所有子节点 */
        NodeList nodes = elt.getChildNodes();
        if (nodes != null) {
            log.trace("发现[" + nodes.getLength() + "]个节点...");

            /* 添加头分隔符 */
            if (this.format.charAt(0) == '1') {
                msg.put(separate.getBytes());
            }
            /* 遍历所有节点 */
            for (int i = 0, n = 0; i < nodes.getLength(); i++) {
                Node nodeChild = nodes.item(i);

                /* 判断子节点是否是元素 */
                if (nodeChild.getNodeType() == Node.ELEMENT_NODE) {
                    /* 添加分隔符 */
                    if ((n++) > 0) {
                        msg.put(separate.getBytes());
                    }
                    log.debug("发现可以处理的节点[" + i + "." + nodeChild.getNodeName() + "]...");
                    String dictName = (((Element) nodeChild).hasAttribute(DICT) ? ((Element) nodeChild).getAttribute(DICT) : "");
                    String format = (((Element) nodeChild).hasAttribute(FORMAT) ? ((Element) nodeChild).getAttribute(FORMAT) : null);

                    /* 判断元素类型 */
                    switch (nodeChild.getNodeName()) {
                        case VALUE:
                            MValue value = (MValue) dict.get(dictName);
                            msg.put(value.getBytes(format, encoding));

                            break;
                        case FIELD:
                            MField field = MField.create((Element) nodeChild, version, encoding);
                            log.info("发现可以处理的" + field.toString());
                            byte[] tmpMsg = field.pack(dict);
                            msg.put(tmpMsg);

                            break;
                        default:
                            break;
                    }

//					msg.put(separate.getBytes());
                }
            }
            /* 添加尾分隔符 */
            if (this.format.charAt(1) == '1') {
                msg.put(separate.getBytes());
            }
        }

        return msg;
    }

    /**
     * @Func:   split
     * @Brief:  分割
     * @Param:  Name    Type   IO  Brief
     *          msg     byte[] IN  报文
     *          sep     byte[] IN  分隔符
     * @Return: 分割后报文列表(ArrayList)
     */
    public static ArrayList<byte[]> split(byte[] msg, byte[] sep) {
        ArrayList<byte[]> list = new ArrayList<byte[]>();

        int n = 0;
        for (int i = 0; i < msg.length; i++) {
            if (msg[i] == sep[0]) {
                for (int j = 0; j < sep.length; j++) {
                    if (msg[i + j] != sep[j]) {
                        break;
                    } else if (j == sep.length - 1) {
                        byte[] tmp = new byte[i - n];
                        System.arraycopy(msg, n, tmp, 0, tmp.length);
                        list.add(tmp);
                        i = i + j;
                        n = i + 1;
                    }
                }
            }
        }

        if (n < msg.length) {
            byte[] tmp = new byte[msg.length - n];
            System.arraycopy(msg, n, tmp, 0, tmp.length);
            list.add(tmp);
        }

        return list;
    }
}