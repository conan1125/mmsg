/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.field.templet
 * @File:       MJoin.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2019-01-04  Conan       新建
 */
package cn.com.dreammill.msg.field.templet;

import cn.com.dreammill.msg.element.MDict;
import cn.com.dreammill.msg.element.MValue;
import cn.com.dreammill.msg.exception.IllicitFormatException;
import cn.com.dreammill.msg.exception.IllicitTypeException;
import cn.com.dreammill.msg.exception.IllicitValueException;
import cn.com.dreammill.msg.exception.LackDomainException;
import cn.com.dreammill.msg.field.MField;
import cn.com.dreammill.msg.field.base.MFastBase;
import cn.com.dreammill.msg.field.base.MVBase;
import cn.com.dreammill.msg.tool.MStringUtils;
import cn.com.dreammill.msg.util.MMsgInfo;
import cn.com.dreammill.msg.util.type.MMsgCfgs;
import org.apache.log4j.Logger;
import org.dom4j.DocumentException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * @Class:  MJoin
 * @Brief;  定长域
 * @Extend: MVBase
 * @Impl:   MFastBase
 */
public class MJoin extends MVBase implements MFastBase {
    private static Logger log = Logger.getLogger(MJoin.class);

    /**
     * @Func:   MJoin
     * @Brief:  构造函数
     * @Param:  Name        Type        IO  Brief
     *          elt         Element     IN  元素
     *          version     String      IN  版本
     *          encoding    String      IN  编码
     */
    public MJoin(Element elt, String version, String encoding) {
        super(elt, version, encoding);
    }

    /**
     * @Func:   identify
     * @Brief:  识别报文
     * @Param:  Name    Type        IO  Brief
     *          msg     byte[]      IN  报文
     *          values  ArrayList   IN  值
     * @Return: 报文信息(MMsgInfo)
     */
    @Override
    public MMsgInfo identify(byte[] msg, ArrayList<String> values)
            throws IllicitValueException
            , IllicitTypeException {
        return identifyByJoin(msg, eltRoot, values);
    }

    /**
     * @Func:   pack
     * @Brief:  组包
     * @Param:  Name    Type   IO  Brief
     *          dict    MDict  IN  字典
     * @Return: 报文(byte[])
     */
    @Override
    public byte[] pack(MDict dict)
            throws IllicitValueException
            , IllicitTypeException
            , IOException
            , IllicitFormatException
            , DocumentException {
        ByteBuffer bf = packByJoin(dict, eltRoot);
        bf.flip();

        byte[] msg = new byte[bf.limit()];
        bf.get(msg);

        return msg;
    }

    /**
     * @Func:   unpack
     * @Brief:  拆包
     * @Param:  Name    Type       IO  Brief
     *          msg     byte[]     IN  报文
     * @Return: 字典(MDict)
     */
    @Override
    public MDict unpack(byte[] msg)
            throws UnsupportedEncodingException
            , IllicitValueException
            , IllicitTypeException
            , LackDomainException
            , IllicitFormatException
            , DocumentException {
        ByteBuffer bf = ByteBuffer.allocate(msg.length);
        bf.put(msg);
        bf.flip();
        return unpackByJoin(bf, eltRoot);
    }

    /**
     * @Func:   identifyByJoin
     * @Brief:  识别报文
     * @Param:  Name    Type        IO  Brief
     *          msg     byte[]      IN  报文
     *          elt     Element     IN  元素
     *          values  ArrayList   IN  值
     * @Return: 报文信息(MMsgInfo)
     */
    protected MMsgInfo identifyByJoin(byte[] msg, Element elt, ArrayList<String> values)
            throws IllicitTypeException
            , IllicitValueException {
        String msgType = spell(values);

        log.debug("报文类型：" + msgType);
        /* 获取所有子节点 */
        NodeList nodes = elt.getChildNodes();
        if(nodes != null) {
            log.trace("发现[" + nodes.getLength() + "]个节点...");
            /* 遍历所有节点 */
            for(int i = 0; i < nodes.getLength(); i++) {
                Node nodeChild = nodes.item(i);

                /* 判断子节点是否是元素 */
                if(nodeChild.getNodeType() == Node.ELEMENT_NODE) {
                    log.debug("发现可以处理的节点[" + i + "." + nodeChild.getNodeName() + "]...");

                    /* 判断元素类型 */
                    switch(nodeChild.getNodeName()) {
                        case VALUE:
                            String format = (((Element) nodeChild).hasAttribute(FORMAT) ? ((Element) nodeChild).getAttribute(FORMAT) : null);
                            String startVal = (((Element) nodeChild).hasAttribute(START) ? ((Element) nodeChild).getAttribute(START) : null);
                            String lengthVal = (((Element) nodeChild).hasAttribute(LENGTH) ? ((Element) nodeChild).getAttribute(LENGTH) : null);
                            int start = (MStringUtils.isExist(startVal) ? Integer.parseInt(startVal) : 0);
                            int length = (MStringUtils.isExist(lengthVal) ? Integer.parseInt(lengthVal) : msg.length - start);

                            byte[] tmp = new byte[length];
                            System.arraycopy(msg, start, tmp, 0, length);
                            MValue value = new MValue(tmp, format, encoding);
                            values.add(value.toString());

                            return identifyByJoin(msg, (Element)nodeChild, values);
                        case FIELD:
                            break;
                        default:
                            if('0' <= msgType.charAt(1)
                                    && msgType.charAt(1) <= '9') {
                                msgType = "_MSG" + msgType;
                            }
                            if(msgType.equalsIgnoreCase("_" + nodeChild.getNodeName() + "_")) {
                                log.info("识别到报文类型[" + msgType + "]...");

                                String explain = ((Element) nodeChild).getAttribute(EXPLAIN);
                                MMsgCfgs msgCfgs = getMsgCFGs((Element) nodeChild);

                                return new MMsgInfo(nodeChild.getNodeName(), explain, msgCfgs);
                            }
                            break;
                    }
                }
            }
        }

        return null;
    }

    /**
     * @Func:   unpackByJoin
     * @Brief:  拆包
     * @Param:  Name    Type       IO  Brief
     *          msg     byte[]     IN  报文
     *          elt     Element    IN  元素
     * @Return: 字典(MDict)
     */
    protected MDict unpackByJoin(ByteBuffer msg, Element elt)
            throws IllicitTypeException
            , IllicitValueException
            , IllicitFormatException
            , UnsupportedEncodingException
            , LackDomainException
            , DocumentException {
        MDict dict = new MDict();

        /* 获取所有子节点 */
        NodeList nodes = elt.getChildNodes();
        if(nodes != null) {
            log.trace("发现[" + nodes.getLength() + "]个节点...");
            /* 遍历所有节点 */
            for(int i = 0; i < nodes.getLength(); i++) {
                Node nodeChild = nodes.item(i);

                /* 判断子节点是否是元素 */
                if(nodeChild.getNodeType() == Node.ELEMENT_NODE) {
                    log.debug("发现可以处理的节点[" + i + "." + nodeChild.getNodeName() + "]...");
                    String dictName = (((Element) nodeChild).hasAttribute(DICT) ? ((Element) nodeChild).getAttribute(DICT) : "");
                    String format = (((Element) nodeChild).hasAttribute(FORMAT) ? ((Element) nodeChild).getAttribute(FORMAT) : null);
                    boolean mask = (((Element) nodeChild).hasAttribute(MASK) ? Boolean.valueOf(((Element) nodeChild).getAttribute(MASK)) : false);
                    String lengthVal = (((Element) nodeChild).hasAttribute(LENGTH) ? ((Element) nodeChild).getAttribute(LENGTH) : null);
                    int length = (MStringUtils.isExist(lengthVal) ? Integer.parseInt(lengthVal) : msg.limit() - msg.position());

                    /* 判断元素类型 */
                    switch(nodeChild.getNodeName()) {
                        case VALUE:
                            byte[] tmp = new byte[length];
                            msg.get(tmp);
                            MValue value = new MValue(tmp, format, encoding, mask);
                            dict.put(dictName, value);
                            msg.mark();
                            break;
                        case FIELD:
                            byte[] newMsg = new byte[length];
                            msg.get(newMsg);

                            MField field = MField.create((Element)nodeChild, version, encoding);
                            log.info("发现可以处理的" + field.toString());
                            MDict tmpDict = field.unpack(newMsg);

                            if(MStringUtils.isExist(dictName)) {
                                dict.put(dictName, tmpDict);
                            } else {
                                dict.putAll(tmpDict);
                            }

                            break;
                        default:
                            break;
                    }
                }
            }
        }

        return dict;
    }

    /**
     * @Func:   packByJoin
     * @Brief:  组包
     * @Param:  Name    Type    IO  Brief
     *          dict    MDict   IN  字典
     *          elt     Element IN  元素
     * @Return: 报文(byte[])
     */
    protected ByteBuffer packByJoin(MDict dict, Element elt)
            throws IllicitFormatException
            , IllicitTypeException
            , IllicitValueException
            , IOException
            , DocumentException {
        ByteBuffer msg = ByteBuffer.allocate(4 * 1024);

        /* 获取所有子节点 */
        NodeList nodes = elt.getChildNodes();
        if(nodes != null) {
            log.trace("发现[" + nodes.getLength() + "]个节点...");
            /* 遍历所有节点 */
            for(int i = 0; i < nodes.getLength(); i++) {
                Node nodeChild = nodes.item(i);

                /* 判断子节点是否是元素 */
                if(nodeChild.getNodeType() == Node.ELEMENT_NODE) {
                    log.debug("发现可以处理的节点[" + i + "." + nodeChild.getNodeName() + "]...");
                    String dictName = (((Element) nodeChild).hasAttribute(DICT) ? ((Element) nodeChild).getAttribute(DICT) : "");
                    String format = (((Element) nodeChild).hasAttribute(FORMAT) ? ((Element) nodeChild).getAttribute(FORMAT) : null);

                    /* 判断元素类型 */
                    switch(nodeChild.getNodeName()) {
                        case VALUE:
                            MValue value = (MValue) dict.get(dictName);
                            msg.put(value.getBytes(format, encoding));

                            break;
                        case FIELD:
                            MField field = MField.create((Element)nodeChild, version, encoding);
                            log.info("发现可以处理的" + field.toString());
                            byte[] tmpMsg = field.pack(dict);
                            msg.put(tmpMsg);

                            break;
                        default:
                            break;
                    }
                }
            }
        }

        return msg;
    }
}
