/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.field.templet
 * @File:       MKV.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2019-01-07  Conan       新建
 */
package cn.com.dreammill.msg.field.templet;

import cn.com.dreammill.msg.element.MDict;
import cn.com.dreammill.msg.exception.IllicitFormatException;
import cn.com.dreammill.msg.exception.IllicitTypeException;
import cn.com.dreammill.msg.exception.IllicitValueException;
import cn.com.dreammill.msg.exception.LackDomainException;
import cn.com.dreammill.msg.field.base.MFastBase;
import cn.com.dreammill.msg.field.base.MKVBase;
import cn.com.dreammill.msg.util.MMsgInfo;
import cn.com.dreammill.msg.util.XMLUtil;
import org.apache.log4j.Logger;
import org.dom4j.DocumentException;
import org.w3c.dom.Element;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Map;

/**
 * @Class:  MXML
 * @Brief;  XML域
 * @Extend: MKVBase
 * @Impl:   MFastBase
 */

public class MXML extends MKVBase implements MFastBase {
    private static Logger log = Logger.getLogger(MXML.class);
    private boolean isHead = true;

    /**
     * @Func:   MXML
     * @Brief:  构造函数
     * @Param:  Name        Type        IO  Brief
     *          elt         Element     IN  元素
     *          version     String      IN  版本
     *          encoding    String      IN  编码
     */
    public MXML(Element elt, String version, String encoding) {
        super(elt, version, encoding);
    }

    /**
     * @Func:   identify
     * @Brief:  识别报文
     * @Param:  Name    Type        IO  Brief
     *          msg     byte[]      IN  报文
     *          values  ArrayList   IN  值
     * @Return: 报文信息(MMsgInfo)
     */
    @SuppressWarnings("unchecked")
    @Override
    public MMsgInfo identify(byte[] msg, ArrayList<String> values)
            throws UnsupportedEncodingException
            , IllicitValueException
            , IllicitTypeException
            , DocumentException {
        /* 解析XML报文 */
        log.trace("解析XML报文...");
        Map<String, Object> map = XMLUtil.pareseXml2Map(new String(msg, encoding), true);
        log.trace("MAP=" + map);
        /* 报文识别 */
        return identifyByKV(map, eltRoot, values);
    }

    /**
     * @Func:   pack
     * @Brief:  组包
     * @Param:  Name    Type   IO  Brief
     *          dict    MDict  IN  字典
     * @Return: 报文(byte[])
     */
    @Override
    public byte[] pack(MDict dict)
            throws IOException {
        /* 获取XML是否需要头 */
        if(eltRoot.hasAttribute(HEAD)) {
            isHead = Boolean.valueOf(eltRoot.getAttribute(HEAD));
        }

        /* 生成XML报文 */
        log.trace("生成XML报文...");
        Map<String, Object> map = packByKV(dict, eltRoot);
        log.trace("MAP=" + map);
        /* 报文组包 */
        String strXML = XMLUtil.formatXml(XMLUtil.pareseMap2Xml(map), encoding);
        if(!isHead) {
            strXML = strXML.substring(strXML.indexOf("<", 2));
        }
        return strXML.getBytes(encoding);
    }

    /**
     * @Func:   unpack
     * @Brief:  拆包
     * @Param:  Name    Type       IO  Brief
     *          msg     byte[]     IN  报文
     * @Return: 字典(MDict)
     */
    @SuppressWarnings("unchecked")
    @Override
    public MDict unpack(byte[] msg)
            throws UnsupportedEncodingException
            , IllicitValueException
            , IllicitTypeException
            , LackDomainException
            , IllicitFormatException
            , DocumentException {
        /* 解析XML报文 */
        log.trace("解析XML报文...");
        Map<String, Object> map = XMLUtil.pareseXml2Map(new String(msg, encoding), true);
        log.trace("MAP=" + map);
        /* 报文拆包 */
        return unpackByKV(map, eltRoot) ;
    }
}