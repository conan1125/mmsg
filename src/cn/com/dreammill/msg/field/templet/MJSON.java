/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.field.templet
 * @File:       MJSON.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2019-01-07  Conan       新建
 */
package cn.com.dreammill.msg.field.templet;

import cn.com.dreammill.msg.element.MDict;
import cn.com.dreammill.msg.exception.IllicitFormatException;
import cn.com.dreammill.msg.exception.IllicitTypeException;
import cn.com.dreammill.msg.exception.IllicitValueException;
import cn.com.dreammill.msg.exception.LackDomainException;
import cn.com.dreammill.msg.field.base.MFastBase;
import cn.com.dreammill.msg.field.base.MKVBase;
import cn.com.dreammill.msg.util.JSONUtil;
import cn.com.dreammill.msg.util.MMsgInfo;
import org.apache.log4j.Logger;
import org.dom4j.DocumentException;
import org.w3c.dom.Element;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Map;

/**
 * @Class:  MJSON
 * @Brief;  JSON域
 * @Extend: MKVBase
 * @Impl:   MFastBase
 */
public class MJSON extends MKVBase implements MFastBase {
    private static Logger log = Logger.getLogger(MJSON.class);

    /**
     * @Func:   MJSON
     * @Brief:  构造函数
     * @Param:  Name        Type        IO  Brief
     *          elt         Element     IN  元素
     *          version     String      IN  版本
     *          encoding    String      IN  编码
     */
    public MJSON(Element elt, String version, String encoding) {
        super(elt, version, encoding);
    }

    /**
     * @Func:   identify
     * @Brief:  识别报文
     * @Param:  Name    Type        IO  Brief
     *          msg     byte[]      IN  报文
     *          values  ArrayList   IN  值
     * @Return: 报文信息(MMsgInfo)
     */
    @Override
    public MMsgInfo identify(byte[] msg, ArrayList<String> values)
            throws UnsupportedEncodingException
            , IllicitValueException
            , IllicitTypeException {
        /* 解析JSON报文 */
        log.trace("解析JSON报文...");
        Map<String, Object> map = JSONUtil.pareseJson2Map(new String(msg, encoding));
        log.trace("MAP=" + map);
        /* 报文识别 */
        return identifyByKV(map, eltRoot, values);
    }

    /**
     * @Func:   pack
     * @Brief:  组包
     * @Param:  Name    Type   IO  Brief
     *          dict    MDict  IN  字典
     * @Return: 报文(byte[])
     */
    @Override
    public byte[] pack(MDict dict)
            throws UnsupportedEncodingException {
        /* 生成JSON报文 */
        log.trace("生成JSON报文...");
        Map<String, Object> map = packByKV(dict, eltRoot);
        log.trace("MAP=" + map);
        /* 报文组包 */
        return JSONUtil.pareseMap2Json(map).getBytes(encoding);
    }

    /**
     * @Func:   unpack
     * @Brief:  拆包
     * @Param:  Name    Type       IO  Brief
     *          msg     byte[]     IN  报文
     * @Return: 字典(MDict)
     */
    @Override
    public MDict unpack(byte[] msg)
            throws
            UnsupportedEncodingException
            , IllicitValueException
            , IllicitTypeException
            , LackDomainException
            , IllicitFormatException
            , DocumentException {
        /* 解析JSON报文 */
        log.trace("解析JSON报文...");
        Map<String, Object> map = JSONUtil.pareseJson2Map(new String(msg, encoding));
        log.trace("MAP=" + map);
        /* 报文拆包 */
        return unpackByKV(map, eltRoot) ;
    }
}
