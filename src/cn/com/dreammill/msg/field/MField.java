/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.field
 * @File:       MField.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2019-01-03  Conan       新建
 *                      2019-01-07  Conan       新增定变长、分隔符、JSON
 *                      2019-01-21  Conan       新增XML、键值对
 */
package cn.com.dreammill.msg.field;

import cn.com.dreammill.msg.element.MDict;
import cn.com.dreammill.msg.exception.IllicitFormatException;
import cn.com.dreammill.msg.exception.IllicitTypeException;
import cn.com.dreammill.msg.exception.IllicitValueException;
import cn.com.dreammill.msg.exception.LackDomainException;
import cn.com.dreammill.msg.field.templet.*;
import cn.com.dreammill.msg.lang.MObject;
import cn.com.dreammill.msg.util.type.MFieldType;
import cn.com.dreammill.msg.util.type.MMsgCfgs;
import org.apache.log4j.Logger;
import org.dom4j.DocumentException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * @Class:  MFied
 * @Brief;  域
 * @Extend: MObject
 */
public abstract class MField extends MObject {
    private static Logger log = Logger.getLogger(MField.class);

    /* 常量 */
    public final static String VALUE = "Value";
    public final static String OBJECT = "Object";
    public final static String ARRAY = "Array";
    public final static String FIELD = "Field";

    public abstract byte[] pack(MDict dict)
            throws UnsupportedEncodingException
            , IllicitValueException
            , IllicitTypeException
            , IOException
            , IllicitFormatException
            , DocumentException;
    public abstract MDict unpack(byte[] msg)
            throws UnsupportedEncodingException
            , IllicitValueException
            , IllicitTypeException
            , LackDomainException
            , IllicitFormatException
            , DocumentException;

    /**
     * @Func	MField
     * @Brief	构造函数
     * @Param	@Type		@IO	@Brief
     * elt		MFieldType	IN	元素
     * version	String		IN	版本
     * encoding	String		IN	编码
     */
    public MField(final Element elt, final String version, final String encoding) {
        init(elt, version, encoding);
    }

    /**
     * @Func	create
     * @Brief	工厂创建器
     * @Param	@Type		@IO	@Brief
     * elt		MFieldType	IN	元素
     * ver		String		IN	版本
     * ecd		String		IN	编码
     * @Throws	IllicitFormatException
     * 			IllicitTypeException
     */
    public static final MField create(final Element elt, final String ver, final String ecd)
            throws IllicitFormatException
            , IllicitTypeException {
        if(elt.hasAttribute(TYPE)) {
            MField field = null;
            String type = elt.getAttribute(TYPE);
            String separate = "";
            String format = "";
            String split = "";
            String text = "";
            log.debug("域类型为:[" + type + "]");

            switch(MFieldType.fromString(type)) {
                // 定变长
                case JOIN:
                    field = new MJoin(elt, ver, ecd);
                    break;
                // 分隔符
                case SEP:
                    separate = (elt.hasAttribute(SEPARATOR) ? elt.getAttribute(SEPARATOR) : " ");
                    format = (elt.hasAttribute(FORMAT) ? elt.getAttribute(FORMAT) : "00");
                    field = new MSeparate(elt, ver, ecd, separate, format);
                    break;
                // JSON格式
                case JSON:
                    field = new MJSON(elt, ver, ecd);
                    break;
                // XML
                case XML:
                    field = new MXML(elt, ver, ecd);
                    break;
                // 键值对
                case KV:
                    separate = (elt.hasAttribute(SEPARATOR) ? elt.getAttribute(SEPARATOR) : "&");
                    split = (elt.hasAttribute(SPLIT) ? elt.getAttribute(SPLIT) : ":");
                    text = (elt.hasAttribute(TEXT) ? elt.getAttribute(TEXT) : "");
                    field = new MKV(elt, ver, ecd, separate, split, text);
                    break;
                default:
                    log.error("无效的域类型:[" + type + "]");
                    throw new IllicitTypeException(type);
            }

            return field;
        } else {
            log.error("未发现域对应的类型！请检查配置文件");
            throw new IllicitFormatException("");
        }
    }

    /**
     * @Func	spell
     * @Brief	拼接
     * @Param	@Type				@IO	@Brief
     * values	ArrayList<String>	IN	元素
     */
    protected String spell(final ArrayList<String> values) {
        StringBuilder sb = new StringBuilder();
        sb.append('_');
        for(String value : values) {
            sb.append(value);
            sb.append('_');
        }

        return sb.toString();
    }

    /**
     * @Func	getMsgCFGs
     * @Brief	获取报文相关配置文件
     * @Param	@Type	@IO	@Brief
     * elt		Element	IN	元素
     */
    protected MMsgCfgs getMsgCFGs(final Element elt) {
        MMsgCfgs msgCfgs = new MMsgCfgs();
        /* 获取所有子节点 */
        NodeList nodes = elt.getChildNodes();
        if(nodes != null) {
            /* 遍历所有节点 */
            for(int i = 0; i < nodes.getLength(); i++) {
                Node nodeChild = nodes.item(i);

                /* 判断子节点是否是元素 */
                if(nodeChild.getNodeType() == Node.ELEMENT_NODE) {
                    if("Cfg".equals(nodeChild.getNodeName())) {
                        msgCfgs.put(((Element) nodeChild).getAttribute(KEY), ((Element) nodeChild).getAttribute(PATH));
                    }
                }
            }
        }
        return msgCfgs;
    }
}
