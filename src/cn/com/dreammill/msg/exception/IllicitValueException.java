/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.exception
 * @File:       IllicitValueException.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2018-12-29  Conan       新建
 */
package cn.com.dreammill.msg.exception;

/**
 * @Enum:       IllicitValueException
 * @Brief:      非法值异常
 * @Extend:     MMsgException
 */
@SuppressWarnings("serial")
public class IllicitValueException extends MMsgException {
    public IllicitValueException(String str) {  super(str); }
}