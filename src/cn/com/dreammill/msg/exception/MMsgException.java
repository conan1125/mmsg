/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.exception
 * @File:       MMsgException.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2018-12-26  Conan       新建
 */
package cn.com.dreammill.msg.exception;

/**
 * @Enum:       MMsgException
 * @Brief:      拆组包异常
 * @Extend:     Exception
 */
@SuppressWarnings("serial")
public abstract class MMsgException extends Exception {
    /**
     * @Func:   MMsgException
     * @Brief:  构造函数
     * @Param:  Name    Type    IO  Brief
     *          str     String  IN  异常提示信息
     */
    public MMsgException(String str) {
        super(str);
    }
}
