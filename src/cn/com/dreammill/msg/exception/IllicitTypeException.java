/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.exception
 * @File:       IllicitTypeException.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2018-12-26  Conan       新建
 */
package cn.com.dreammill.msg.exception;

/**
 * @Enum:       IllicitTypeException
 * @Brief:      非法类型异常
 * @Extend:     MMsgException
 */
@SuppressWarnings("serial")
public class IllicitTypeException extends MMsgException {
    /**
     * @Func:   IllicitTypeException
     * @Brief:  构造函数
     * @Param:  Name    Type    IO  Brief
     *          type    String  IN  类型
     */
    public IllicitTypeException(String type) {
        super("[" + type + "] is Illicit Type");
    }
}
