/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.exception
 * @File:       IllicitFormatException.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2018-12-29  Conan       新建
 */
package cn.com.dreammill.msg.exception;

/**
 * @Enum:       IllicitFormatException
 * @Brief:      非法格式异常
 * @Extend:     MMsgException
 */
@SuppressWarnings("serial")
public class IllicitFormatException extends MMsgException {
    /**
     * @Func:   IllicitFormatException
     * @Brief:  构造函数
     * @Param:  Name    Type    IO  Brief
     *          type    String  IN  类型
     */
    public IllicitFormatException(String type) {    super("[" + type + "] is Illicit Format");  }
}