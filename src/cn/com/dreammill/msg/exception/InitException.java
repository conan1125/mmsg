/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.exception
 * @File:       InitException.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2018-12-28  Conan       新建
 */
package cn.com.dreammill.msg.exception;

/**
 * @Enum:       InitException
 * @Brief:      初始化异常
 * @Extend:     MMsgException
 */
@SuppressWarnings("serial")
public class InitException extends MMsgException {
    /**
     * @Func:   InitException
     * @Brief:  构造函数
     * @Param:  Name    Type    IO  Brief
     *          str     String  IN  初始化对象名称
     */
    public InitException(String str) {
        super("Load [" + str + "] fail!");
    }
}
