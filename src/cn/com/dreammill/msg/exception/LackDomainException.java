/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.exception
 * @File:       LackDomainException.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2018-12-29  Conan       新建
 */
package cn.com.dreammill.msg.exception;

/**
 * @Enum:       LackDomainException
 * @Brief:      缺少域异常
 * @Extend:     MMsgException
 */
@SuppressWarnings("serial")
public class LackDomainException extends MMsgException {
    public LackDomainException(String str) {    super("Not found [" + str + "]!");  }
}