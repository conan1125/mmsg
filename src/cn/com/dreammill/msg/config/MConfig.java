/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.config
 * @File:       MConfig.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2018-12-28  Conan       新建
 */
package cn.com.dreammill.msg.config;

import cn.com.dreammill.msg.exception.InitException;
import cn.com.dreammill.msg.util.type.MFormat;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.Properties;

/**
 * @Enum:       MConfig
 * @Brief:      拆组包配置
 *              单例模式
 */
public class MConfig {
    private static Logger log = Logger.getLogger(MFormat.class);
    /* 常量 */
    private static final String CFG_DIR = "DIR";
    private static final String SIR = "SIR";
    /* 静态变量 */
    private static MConfig instance = new MConfig();
    private static String cfgDir = "";
    private static boolean isSIE = false;

    /**
     * @Func:   MConfig
     * @Brief:  私有构造函数
     */
    private MConfig() {};

    /**
     * @Func:   init
     * @Brief:  初始化函数
     * @Param:  Name    Type    IO  Brief
     *          cfgName String  IN  配置文件名
     * @Except: IOException
     *          InitException
     */
    public static void init(final String cfgName)
            throws IOException, InitException {
        log.info("==> 加载报文配置 ==>");
        Properties ppt = load(cfgName);
        if(ppt == null) {
            log.error("加载配置文件失败！");
            throw new InitException(cfgName);
        }
        init(ppt);
    }

    /**
     * @Func:   init
     * @Brief:  初始化函数
     * @Param:  Name    Type        IO  Brief
     *          ppt     Properties  IN  配置文件
     * @Except: IOException
     *          InitException
     */
    @SuppressWarnings("static-access")
    public static void init(final Properties ppt)
            throws IOException, InitException {
        if(ppt.containsKey(CFG_DIR)) {
            /* 判断目录是否存在 */
            String cfg = ppt.getProperty(CFG_DIR);
            String[] beginDirs =
                    {
                            "",
                            System.getProperty("user.dir") + "/",
                            new File("").getAbsolutePath() + "/",
                            System.getProperty("catalina.home") + "/"
                    };

            for(String beginDir : beginDirs) {
                if(new File(beginDir + cfg).exists()) {
                    instance.cfgDir = beginDir + cfg;
                    log.info("发现目录：" + beginDir + cfg);
                    break;
                } else {
                    log.warn("未发现目录：" + beginDir + cfg);
                }
            }

            log.info("通用配置文件路径:" + instance.cfgDir);
        }
        if(ppt.containsKey(SIR)) {
            instance.isSIE = Boolean.valueOf(ppt.getProperty(SIR));
            log.info("敏感信息是否处理:" + instance.isSIE);
        }
    }

    /**
     * @Func:   isSIE
     * @Brief:  是否屏蔽敏感信息
     * @Return: 是否屏蔽敏感信息（boolean）
     */
    @SuppressWarnings("static-access")
    public static boolean isSIE() {
        return instance.isSIE;
    }

    /**
     * @Func:   getCfgDir
     * @Brief:  获取配置文件目录
     * @Return: 配置文件目录（String）
     */
    @SuppressWarnings("static-access")
    public static String getCfgDir() {
        return instance.cfgDir;
    }

    /**
     * @Func:   load
     * @Brief:  加载配置文件
     * @Param:  Name    Type    IO  Brief
     *          cfgName String  IN  配置文件名
     * @Return: 配置文件（Properties）
     */
    private static Properties load(final String cfgName) {

        String path = System.getProperty("user.dir");
        String fileName = path + "/" +  (cfgName.indexOf('/') == 0 ? cfgName.substring(1) : cfgName);
        File file = new File(fileName);
        Properties prop = new Properties();

        if(file.exists()) {
            InputStream in;
            try {
                in = new BufferedInputStream(new FileInputStream(file));
                prop.load(in);
                return prop;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            log.warn("Not found [" + fileName + "]");
        }

        return null;
    }
}
