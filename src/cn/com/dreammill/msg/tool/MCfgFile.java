/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.tool
 * @File:       MCfgFile.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2018-12-29  Conan       新建
 */
package cn.com.dreammill.msg.tool;

import java.io.File;

import cn.com.dreammill.msg.config.MConfig;
import org.apache.log4j.Logger;

/**
 * @Enum:       MCfgFile
 * @Brief:      拆组包配置文件类
 * @Entend:     java.io.File
 */
@SuppressWarnings("serial")
public class MCfgFile extends File {
    private static Logger log = Logger.getLogger(MCfgFile.class);								                    // 日志

    /**
     * @Func:   MCfgFile
     * @Brief:  构造函数
     * @Param:  Name        Type    IO  Brief
     *          pathname    String  IN  字符数组
     */
    public MCfgFile(final String pathname) {
        super(MConfig.getCfgDir() + pathname);
        log.debug("配置文件名称：" + this.getName());
    }
}
