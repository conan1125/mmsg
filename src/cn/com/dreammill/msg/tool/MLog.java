/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.tool
 * @File:       MLog.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2018-12-28  Conan       新建
 */
package cn.com.dreammill.msg.tool;

import cn.com.dreammill.msg.config.MConfig;

/**
 * @Enum:       MLog
 * @Brief:      日志类
 */
public class MLog {
    private static final String symbol = "*";

    /**
     * @Func:   formatHex
     * @Brief:  格式化16进制
     * @Param:  Name    Type    IO  Brief
     *          msg     byte[]  IN  原报文
     * @Return: 格式化后报文（String）
     */
    public static String formatHex(byte[] msg) {
        if(MConfig.isSIE()) {
            return	"\n" +
                    MBytes.formatHex(msg, false);
        } else {
            return	"\n" +
                    MBytes.formatHex(msg, true) +
                    (msg == null ? "" : new String(msg));
        }
    }

    public static String dataMask(String secretInfo){
        String maskedinfo = "";
//        if(StringUtils.isBlank(secretInfo)){
//            return "";
//        }else {
//            int len = secretInfo.length();
//            if (len >= 2 && len <= 4) {
//                String fstCh = StringUtils.left(secretInfo, 1);
//                maskedinfo = StringUtils.rightPad(fstCh, len, symbol);
//            }
//            else if (len > 4) {
//                maskedinfo = StringUtils.rightPad(StringUtils.left(secretInfo, len - 4), len, symbol);
//            }else {
//                return secretInfo;
//            }
//            return  maskedinfo;
//        }
        if(MStringUtils.isExist(secretInfo)) {
            int len = secretInfo.length();
            int begin = 0;
            int end = len;

            /* 遮蔽规则：
             *  1个字时：全部遮挡
             *  2~5个字时：后面一半的字符
             *  6~8个字时：正数二到倒数二之间的字符
             *  大于8个字时：倒数三之前的4个字符
             */
            if(len <= 1) {
                begin = 0;
                end = len;
            } else if(2 <= len && len <= 5) {
                begin = len - (len / 2);
                end = len;
            } else if(6 <= len && len <= 8) {
                begin = 2;
                end = len - 2;
            } else {
                begin = len - 6;
                end = len - 2;
            }

            StringBuilder sb = new StringBuilder(len);
            if(begin != 0) {
                sb.append(secretInfo.substring(0, begin));
            }
            for(int i = begin; i < end; i++) {
                sb.append("*");
            }
            if(end != len) {
                sb.append(secretInfo.substring(end, len));
            }

            return sb.toString();
        } else {
            return "";
        }
    }

    public static void main(String[] args) throws Exception {
        String str = "一二三四五六七八九十";

        for(int i = 1; i <= 10; i++) {
            String test = str.substring(0, i);
            System.out.println("[" + test + "]加密后[" + MLog.dataMask(test) + "]");
        }
    }
}
