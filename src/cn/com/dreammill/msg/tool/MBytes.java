/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.tool
 * @File:       MBytes.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2018-12-28  Conan       新建
 */
package cn.com.dreammill.msg.tool;

import cn.com.dreammill.msg.util.type.MAlign;

/**
 * @Enum:       MBytes
 * @Brief;      字符数组工具
 */
@SuppressWarnings("unused")
public class MBytes {
    /**
     * @Func:   toHex
     * @Brief:  转16进制
     * @Param:  Name    Type    IO  Brief
     *          val     byte[]  IN  字符数组
     * @Return: 转16进制结果（String）
     */
    public static String toHex(final byte[] val) {
        StringBuilder out = new StringBuilder();

        for(byte b : val) {
            out.append(String.format("%02X ", b));
        }

        return out.toString();
    }

    /**
     * @Func:   formatHex
     * @Brief:  将字符串格式化为标准16进制
     * @Param:  Name        Type    IO  Brief
     *          bytes       byte[]  IN  报文
     *          isPrintMsg  boolean IN  是否打印原报文
     * @Return: 格式化后输出（String）
     */
    public static String formatHex(final byte[] bytes, final boolean isPrintMsg) {
        /* 头 */
        StringBuilder out = new StringBuilder();
        out.append("Displacement -0--1--2--3--4--5--6--7--8--9--A--B--C--D--E--F --ASCII Value--\n");

        if(bytes == null || bytes.length == 0) {
            return out.toString();
        }

        byte[] b = new byte[bytes.length];
        System.arraycopy(bytes, 0, b, 0, bytes.length);

        /* 遍历所有字符 */
        for(int i = 0; i < b.length; i++) {
            for(int j = 0; j <= (b.length - 1) /16; j++) {
                int n;																			//本行的字符个数
                out.append(String.format("%05d(%05X) ", j * 16, j * 16));

                /* 每16个为一行，显示16进制 */
                for(n = 0; n < 16 && i < b.length; i++, n++) {
                    out.append(String.format("%02X ", b[i]));

                    switch(b[i]) {
                        case 0x00:
                        case 0x0A:
                            b[i] = ' ';
                            break;
                    }
                }
                for(int k = 0; k < 16 - n; k++) {
                    out.append("   ");
                }

                if(isPrintMsg) {
                    /* 显示16进制对应的ASCII */
                    byte[] print = new byte[16];
                    for(int k = 0; k < n; k++) {
                        if(31 < b[i - n + k]
                                && b[i - n + k] < 127) {
                            print[k] = b[i - n + k];
                        } else {
                            print[k] = '.';

                        }
                    }

                    out.append(new String(print));
                }
                out.append('\n');
            }
        }

        return out.toString();
    }

    /**
     * @Func:   bcd2str
     * @Brief:  BCD码转字符串
     * @Param:  Name    Type    IO  Brief
     *          bytes   byte[]  IO  报文
     *          length  int     IN  是否打印原报文
     * @Return: 转码后字符串（String）
     */
    public static String bcd2str(byte[] bytes, final int length) {
        if (bytes == null) {	bytes = "".getBytes();	}
        return bcd2str(bytes, 0, length);
    }

    /**
     * @Func:   bcd2str
     * @Brief:  BCD码转字符串
     * @Param:  Name    Type    IO  Brief
     *          bytes   byte[]  IO  报文
     *          start   int     IN  开始位置
     *          length  int     IN  是否打印原报文
     * @Return: 转码后字符串（String）
     */
    public static String bcd2str(byte[] bytes, final int start, final int length) {
        int end;
        if(start + length > bytes.length) {
            end = start + bytes.length;
        } else {
            end = start + length;
        }

        char temp[] = new char[length * 2];
        char val;

        for (int i = start; i < end; i++) {
            val = (char) (((bytes[i] & 0xf0) >> 4) & 0x0f);
            temp[(i - start) * 2] = (char) (val > 9 ? val + 'A' - 10 : val + '0');
            val = (char) (bytes[i] & 0x0f);
            temp[(i - start) * 2 + 1] = (char) (val > 9 ? val + 'A' - 10 : val + '0');
        }

        return new String(temp);
    }

    /**
     * @Func:   str2Bcd
     * @Brief:  字符串转BCD码
     * @Param:  Name    Type    IO  Brief
     *          asc     String  IO  报文
     *          align   MAlign  IN  对齐方向
     * @Return: 转码后BCD码（byte[]）
     */
    public static byte[] str2Bcd(String asc, final MAlign align) {
        int len = asc.length();
        int mod = len % 2;
        if (mod != 0) {
            switch(align) {
                case LEFT:
                    asc = asc + "0";
                    len = asc.length();
                    break;
                default:
                    asc = "0" + asc;
                    len = asc.length();
                    break;
            }
        }
        if (len >= 2) {
            len = len / 2;
        }
        byte abt[];
        byte bbt[] = new byte[len];
        abt = asc.getBytes();
        int j, k;
        for (int p = 0; p < asc.length() / 2; p++) {
            if (abt[2 * p] == '=') {
                j = 'D' - 'A' + 0x0a;
            } else if ((abt[2 * p] >= '0') && (abt[2 * p] <= '9')) {
                j = abt[2 * p] - '0';
            } else if ((abt[2 * p] >= 'a') && (abt[2 * p] <= 'z')) {
                j = abt[2 * p] - 'a' + 0x0a;
            } else if ((abt[2 * p] >= 'A') && (abt[2 * p] <= 'Z')) {
                j = abt[2 * p] - 'A' + 0x0a;
            } else {
                j = 0x00;
            }

            if(abt[2 * p + 1] == '=') {
                k = 'D' - 'A' + 0x0a;
            } else if ((abt[2 * p + 1] >= '0') && (abt[2 * p + 1] <= '9')) {
                k = abt[2 * p + 1] - '0';
            } else if ((abt[2 * p + 1] >= 'a') && (abt[2 * p + 1] <= 'z')) {
                k = abt[2 * p + 1] - 'a' + 0x0a;
            } else if ((abt[2 * p + 1] >= 'A') && (abt[2 * p + 1] <= 'Z')) {
                k = abt[2 * p + 1] - 'A' + 0x0a;
            } else {
                k = 0x00;
            }
            int a = (j << 4) + k;
            byte b = (byte) a;
            bbt[p] = b;
        }
        return bbt;
    }

    /**
     * @Func:   find
     * @Brief:  查找字符数组出现位置
     * @Param:  Name    Type    IO  Brief
     *          msg     byte[]  IN  被查找字符串
     *          start   int     IN  开始位置
     *          bytes   byte[]  IN  查找目标字符串
     * @Return: 字符数组出现位置（int）
     */
    public static int find(final byte[] msg, final int start, final byte[] bytes) {
        /* 判断数组全部存在 */
        if(msg == null
                || bytes == null
                || msg.length == 0
                || bytes.length == 0) {
            return -1;
        }
        /* 判断数组长度是否正确 */
        if(msg.length < start + bytes.length) {
            return -1;
        }

        int i, j;
        /* 从代查询字符数组开始查询位置逐个查询 */
        for (i = start; i < msg.length - bytes.length; i++) {
            /* 与查询字符数组的第一个字符相同后开始查询后面是否一致 */
            if(msg[i] == bytes[0]) {
                /* 判断后面所有字符是否均相同 */
                for(j = 1; j < bytes.length; j++) {
                    if (msg[i + j] != bytes[j]) {
                        break;
                    }
                }
                /* 判断是否所有字符均已比对完毕，如果完毕则返回当前位置 */
                if(j == bytes.length) {
                    return i;
                }
            }
        }
        return -1;
    }
}
