/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.tool
 * @File:       MStringUtils.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2018-12-26  Conan       新建
 */
package cn.com.dreammill.msg.tool;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Enum:       MStringUtils
 * @Brief:      字符串工具
 */
@SuppressWarnings("unused")
public class MStringUtils {
    /**
     * @Func:   isExist
     * @Brief:  字符串是否存在
     *          字符串为NULL或字符串为""均算不存在
     * @Param:  Name    Type    IO  Brief
     *          str     String  IN  字符串
     * @Return: 字符串是否存在（boolean）
     */
    public static boolean isExist(final String str) {
        return (str != null && str.length() > 0 ? true : false);
    }

    public static int appearNumber(String srcText, String findText) {
        int count = 0;
        Pattern p = Pattern.compile(findText);
        Matcher m = p.matcher(srcText);
        while (m.find()) {
            count++;
        }
        return count;
    }

    /**
     * @Func:   toType
     * @Brief:  根据字符串类型将字符串转换为对应类型的值
     * @Param:  Name    Type    IO  Brief
     *          type    String  IN  目标类型
     *          type    String  IN  值
     * @Return: 转换后的值（Object）
     */
    public static Object toType(final String type, final String value) {
        switch(type) {
            case "String":
            case "java.lang.String":
                return java.lang.String.valueOf(value);
            case "byte":
            case "java.lang.Byte":
                return java.lang.Byte.valueOf(value);
            case "short":
            case "java.lang.Short":
                return java.lang.Short.valueOf(value);
            case "int":
            case "java.lang.Integer":
                return java.lang.Integer.valueOf(value);
            case "long":
            case "java.lang.Long":
                return java.lang.Long.valueOf(value);
            case "float":
            case "java.lang.Float":
                return java.lang.Float.valueOf(value);
            case "double":
            case "java.lang.Double":
                return java.lang.Double.valueOf(value);
            case "boolean":
            case "java.lang.Boolean":
                return java.lang.Boolean.valueOf(value);
            case "char":
            case "java.lang.Character":
                return java.lang.Character.valueOf((char) value.getBytes()[0]);
            default:
                return null;
        }
    }
}
