/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.util
 * @File:       XMLUtil.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2019-01-07  Conan       新建
 */
package cn.com.dreammill.msg.util;

import org.dom4j.*;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import java.io.IOException;
import java.io.StringWriter;
import java.util.*;

/**
 * @Enum:       XMLUtil
 * @Brief;      XML工具包
 */
public class XMLUtil {
    /**
     * @Func:   pareseXml2Map
     * @Brief:  解析XML到MAP
     * @Param:  Name        Type        IO  Brief
     *          jsonStr     String      IN  XML字符串
     *          needRootKey boolean     IN  是否有root节点
     * @Return: 解析后的MAP(Map)
     */
    public static Map pareseXml2Map(String xmlStr, boolean needRootKey)
            throws DocumentException {
        /* 解析XML文件 */
        Document doc = DocumentHelper.parseText(xmlStr);
        Element root = doc.getRootElement();
        /* XML转MAP */
        Map<String, Object> map = (Map<String, Object>)pareseXml2Map(root);
        /* 处理ROOT节点 */
        return procRootKey(map, needRootKey, root);
    }

    /**
     * @Func:   pareseXml2MapWithAttr
     * @Brief:  解析XML到MAP
     * @Param:  Name        Type        IO  Brief
     *          jsonStr     String      IN  XML字符串
     *          needRootKey boolean     IN  是否有root节点
     * @Return: 解析后的MAP(Map)
     */
    public static Map pareseXml2MapWithAttr(String xmlStr, boolean needRootKey)
            throws DocumentException {
        /* 解析XML文件 */
        Document doc = DocumentHelper.parseText(xmlStr);
        Element root = doc.getRootElement();
        /* XML转MAP */
        Map<String, Object> map = (Map<String, Object>) pareseXml2MapWithAttr(root);
        /* 处理ROOT节点 */
        return procRootKey(map, needRootKey, root);
    }

    /**
     * @Func:   pareseXml2Map
     * @Brief:  解析XML到MAP
     * @Param:  Name    Type        IO  Brief
     *          e       Element     IN  元素
     * @Return: 解析后的MAP(Map)
     */
    private static Map pareseXml2Map(Element e) {
        Map map = new LinkedHashMap();
        /* 获取所有子元素 */
        List list = e.elements();

        /*
         * 存在多个子元素
         * 不存在多个子元素
         */
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                Element iter = (Element) list.get(i);
                List mapList = new ArrayList();

                if (iter.elements().size() > 0) {
                    Map m = pareseXml2Map(iter);
                    if (map.get(iter.getName()) != null) {
                        Object obj = map.get(iter.getName());

                        if (obj instanceof List) {
                            mapList = (List) obj;
                            mapList.add(m);
                        } else {
                            mapList = new ArrayList();
                            mapList.add(obj);
                            mapList.add(m);
                        }

                        map.put(iter.getName(), mapList);
                    } else {
                        map.put(iter.getName(), m);
                    }
                } else {
                    if (map.get(iter.getName()) != null) {
                        Object obj = map.get(iter.getName());
                        if (!(obj instanceof List)) {
                            mapList = new ArrayList();
                            mapList.add(obj);
                            mapList.add(iter.getText());
                        }
                        if (obj instanceof List) {
                            mapList = (List) obj;
                            mapList.add(iter.getText());
                        }
                        map.put(iter.getName(), mapList);
                    } else {
                        map.put(iter.getName(), iter.getText());
                    }
                }
            }
        } else {
            map.put(e.getName(), e.getText());
        }

        return map;
    }

    /**
     * @Func:   pareseXml2MapWithAttr
     * @Brief:  解析XML到MAP
     * @Param:  Name    Type        IO  Brief
     *          e       Element     IN  元素
     * @Return: 解析后的MAP(Map)
     */
    private static Map pareseXml2MapWithAttr(Element element) {
        Map<String, Object> map = new LinkedHashMap<String, Object>();

        List<Element> list = element.elements();
        // 当前节点的所有属性的list
        List<Attribute> listAttr0 = element.attributes();

        for (Attribute attr : listAttr0) {
            map.put("@" + attr.getName(), attr.getValue());
        }
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                Element iter = list.get(i);
                List mapList = new ArrayList();

                if (iter.elements().size() > 0) {
                    Map m = pareseXml2MapWithAttr(iter);
                    if (map.get(iter.getName()) != null) {
                        Object obj = map.get(iter.getName());

                        if (obj instanceof List) {
                            mapList = (List) obj;
                            mapList.add(m);
                        } else {
                            mapList = new ArrayList();
                            mapList.add(obj);
                            mapList.add(m);
                        }
                        map.put(iter.getName(), mapList);
                    } else {
                        map.put(iter.getName(), m);
                    }
                } else {
                    List<Attribute> listAttr = iter.attributes(); // 当前节点的所有属性的list
                    Map<String, Object> attrMap = null;
                    boolean hasAttributes = false;

                    if (listAttr.size() > 0) {
                        hasAttributes = true;
                        attrMap = new LinkedHashMap<String, Object>();
                        for (Attribute attr : listAttr) {
                            attrMap.put("@" + attr.getName(), attr.getValue());
                        }
                    }

                    if (map.get(iter.getName()) != null) {
                        Object obj = map.get(iter.getName());
                        if (obj instanceof List) {
                            mapList = (List) obj;
                            if (hasAttributes) {
                                attrMap.put("#text", iter.getText());
                                mapList.add(attrMap);
                            } else {
                                mapList.add(iter.getText());
                            }
                        } else {
                            mapList = new ArrayList();
                            mapList.add(obj);
                            // mapList.add(iter.getText());
                            if (hasAttributes) {
                                attrMap.put("#text", iter.getText());
                                mapList.add(attrMap);
                            } else {
                                mapList.add(iter.getText());
                            }
                        }
                        map.put(iter.getName(), mapList);
                    } else {
                        if (hasAttributes) {
                            attrMap.put("#text", iter.getText());
                            map.put(iter.getName(), attrMap);
                        } else {
                            map.put(iter.getName(), iter.getText());
                        }
                    }
                }
            }
        } else {
            // 根节点的
            if (listAttr0.size() > 0) {
                map.put("#text", element.getText());
            } else {
                map.put(element.getName(), element.getText());
            }
        }

        return map;
    }

    /**
     * @Func:   pareseMap2Xml
     * @Brief:  解析XML到XML
     * @Param:  Name        Type    IO  Brief
     *          map         Map     IN  元素
     *          rootName    String  IN  ROOT节点名
     * @Return: 处理后的XML(Document)
     */
    public static Document pareseMap2Xml(Map<String, Object> map, String rootName) {
        Document doc = DocumentHelper.createDocument();
        Element root = DocumentHelper.createElement(rootName);
        doc.add(root);
        pareseMap2Xml(map, root);

        return doc;
    }

    /**
     * @Func:   pareseMap2Xml
     * @Brief:  解析XML到XML
     * @Param:  Name        Type    IO  Brief
     *          map         Map     IN  元素
     * @Return: 处理后的XML(Document)
     */
    public static Document pareseMap2Xml(Map<String, Object> map) {
        Iterator<Map.Entry<String, Object>> entries = map.entrySet().iterator();
        if(entries.hasNext()){
            //获取第一个键创建根节点
            Map.Entry<String, Object> entry = entries.next();
            Document doc = DocumentHelper.createDocument();
            Element root = DocumentHelper.createElement(entry.getKey());
            doc.add(root);
            pareseMap2Xml((Map)entry.getValue(), root);
            return doc;
        }
        return null;
    }

    /**
     * @Func:   pareseMap2Xml
     * @Brief:  解析XML到XML
     * @Param:  Name        Type    IO  Brief
     *          map         Map     IN  元素
     *          body        Element IN  元素
     * @Return: 处理后的XML(Element)
     */
    private static Element pareseMap2Xml(Map<String, Object> map, Element body) {
        Iterator<Map.Entry<String, Object>> entries = map.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry<String, Object> entry = entries.next();
            String key = entry.getKey();
            Object value = entry.getValue();
            if(key.startsWith("@")){//属性
                body.addAttribute(key.substring(1, key.length()), value.toString());
            } else if(key.equals("#text")){ //有属性时的文本
                body.setText(value.toString());
            } else {
                if(value instanceof java.util.List ){
                    List list = (List)value;
                    Object obj;
                    for(int i=0; i<list.size(); i++) {
                        obj = list.get(i);
                        //list里是map或String，不会存在list里直接是list的，
                        if(obj instanceof java.util.Map) {
                            Element subElement = body.addElement(key);
                            pareseMap2Xml((Map)list.get(i), subElement);
                        } else {
                            body.addElement(key).setText((String)list.get(i));
                        }
                    }
                } else if(value instanceof java.util.Map ) {
                    Element subElement = body.addElement(key);
                    pareseMap2Xml((Map)value, subElement);
                } else {
                    body.addElement(key).setText(value.toString());
                }
            }
        }
        return body;
    }

    /**
     * @Func:   formatXml
     * @Brief:  格式化XML报文
     * @Param:  Name        Type    IO  Brief
     *          xmlStr      String  IN  XML报文
     *          encoding    String  IN  编码
     * @Return: XML报文(String)
     */
    public static String formatXml(String xmlStr, String encoding) throws DocumentException, IOException{
        Document document = DocumentHelper.parseText(xmlStr);
        return formatXml(document, encoding);
    }

    /**
     * @Func:   formatXml
     * @Brief:  格式化XML报文
     * @Param:  Name        Type        IO  Brief
     *          document    Document    IN  XML结构
     *          encoding    String      IN  编码
     * @Return: XML报文(String)
     */
    public static String formatXml(final Document document, String encoding)
            throws IOException{
        /* 格式化输出格式 */
        OutputFormat format = OutputFormat.createPrettyPrint();
        format.setEncoding(encoding);
        StringWriter writer = new StringWriter();
        /* 格式化输出流 */
        XMLWriter xmlWriter = new XMLWriter(writer, format);
        /* 将document写入到输出流 */
        xmlWriter.write(document);
        xmlWriter.close();
        return writer.toString();
    }

    /**
     * @Func:   procRootKey
     * @Brief:  格式化XML报文
     * @Param:  Name        Type    IO  Brief
     *          map         Map     IN  MAP键值对
     *          needRootKey boolean IN  是否包含ROOT节点
     *          root        Element IN  root节点
     * @Return: XML报文(String)
     */
    private static Map procRootKey(final Map<String, Object> map, final boolean needRootKey, final Element root) {
        if(root.elements().size() == 0
                && root.attributes().size() == 0){
            return map;
        }
        if(needRootKey){
            Map<String, Object> rootMap = new HashMap<String, Object>();
            rootMap.put(root.getName(), map);
            return rootMap;
        }

        return map;
    }
}