/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.util
 * @File:       MMsgInfo.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2019-01-03  Conan       新建
 */
package cn.com.dreammill.msg.util;

import cn.com.dreammill.msg.util.type.MMsgCfgs;

/**
 * @Enum:       MMsgInfo
 * @Brief;      拆组包信息
 */
public class MMsgInfo {
    private String msgType = null;
    private String msgName = null;
    private MMsgCfgs msgCfgs = null;

    /**
     * @Func:   MMsgInfo
     * @Brief:  构造函数
     * @Param:  Name    Type        IO  Brief
     *          type    String      IN  类型
     *          name    String      IN  名称
     *          cfgs    MMsgCfgs    IN  配置文件
     */
    public MMsgInfo(String type, String name, MMsgCfgs cfgs) {
        this.msgType = type;
        this.msgName = name;
        this.msgCfgs = cfgs;
    }

    public String getCfg(String name) {
        return msgCfgs.get(name);
    }

    public String getName() {
        return msgName;
    }

    public String getType() {
        return msgType;
    }

    public MMsgCfgs getCfgs() {
        return msgCfgs;
    }

    public String toString() {
        return msgName + "[" + msgType + "]";
    }
}
