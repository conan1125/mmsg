/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.util.type
 * @File:       MAlign.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2018-12-24  Conan       新建
 */
package cn.com.dreammill.msg.util.type;

/**
 * @Enum:       MAlign
 * @Brief:      方向
 */
@SuppressWarnings("unused")
public enum MAlign {
    LEFT,RIGHT
}
