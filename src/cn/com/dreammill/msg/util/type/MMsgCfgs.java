/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.util.type
 * @File:       MMsgCfgs.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2018-12-29  Conan       新建
 */
package cn.com.dreammill.msg.util.type;

import java.util.Hashtable;

/**
 * @Enum:       MMappedType
 * @Brief:      映射类型
 * @Extend:     java.util.Hashtable
 */
@SuppressWarnings("serial")
public class MMsgCfgs extends Hashtable<String, String> {
}
