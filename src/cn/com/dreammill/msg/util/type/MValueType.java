/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.util.type
 * @File:       MValueType.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2018-12-26  Conan       新建
 */
package cn.com.dreammill.msg.util.type;

import java.util.HashMap;
import java.util.Map;

/**
 * @Enum:       MMappedType
 * @Brief:      映射类型
 */
@SuppressWarnings("unused")
public enum MValueType {
    s,                                                                                                                  // 字符串
    d,                                                                                                                  // 整形
    f,                                                                                                                  // 浮点型
    b, B,                                                                                                               // ＢＣＤ
    x, X;                                                                                                               // 16进制

    /* 字符串转类型字典 */
    private static final Map<String, MValueType> stringToEnum = new HashMap<>();

    /* 初始化字符串转类型字典 */
    static {
        for(MValueType blah : values()) {
            stringToEnum.put(blah.toString(), blah);
        }
    }

    /**
     * @Func:   fromString
     * @Brief:  字符串转类型
     * @Param:  Name    Type    IO  Brief
     *          symbol  String  IN  类型名
     * @Return: 映射类型（MValueType）
     */
    public static MValueType fromString(String symbol) {
        return stringToEnum.get(symbol);
    }

    /**
     * @Func:   fromChar
     * @Brief:  字符串转类型
     * @Param:  Name    Type    IO  Brief
     *          symbol  char    IN  类型名
     * @Return: 映射类型（MValueType）
     */
    public static MValueType fromChar(char symbol) {
        return stringToEnum.get(String.valueOf(symbol));
    }
}
