/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.util.type
 * @File:       MMappedType.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2018-12-24  Conan       新建
 */
package cn.com.dreammill.msg.util.type;

import java.util.HashMap;
import java.util.Map;

/**
 * @Enum:       MMappedType
 * @Brief:      映射类型
 */
@SuppressWarnings("unused")
public enum MMappedType {
    Func,                                                                                                               // 方法
    Copy;                                                                                                               // 拷贝

    /* 字符串转类型字典 */
    private static final Map<String, MMappedType> stringToEnum = new HashMap<>();

    /* 初始化字符串转类型字典 */
    static {
        for(MMappedType blah : values()) {
            stringToEnum.put(blah.toString(), blah);
        }
    }

    /**
     * @Func:   fromString
     * @Brief:  字符串转类型
     * @Param:  Name    Type    IO  Brief
     *          symbol  String  IN  类型名
     * @Return: 映射类型（MMappedType）
     */
    public static MMappedType fromString(String symbol) {
        return stringToEnum.get(symbol);
    }
}
