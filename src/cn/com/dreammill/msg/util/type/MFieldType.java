/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.util.type
 * @File:       MFieldType.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2018-12-24  Conan       新建
 */
package cn.com.dreammill.msg.util.type;

import java.util.HashMap;
import java.util.Map;

/**
 * @Enum:       MFieldType
 * @Brief:      域类型
 */
@SuppressWarnings("unused")
public enum MFieldType {
    JOIN,	                                                                                                            // 拼接
    SEP,	                                                                                                            // 分隔符
    JSON,	                                                                                                            // JSON
    XML,	                                                                                                            // XML
    KV,		                                                                                                            // Key Value
    TLV;	                                                                                                            // Key Long Value

    /* 字符串转类型字典 */
    private static final Map<String, MFieldType> stringToEnum = new HashMap<>();

    /* 初始化字符串转类型字典 */
    static {
        for(MFieldType blah : values()) {
            stringToEnum.put(blah.toString(), blah);
        }
    }

    /**
     * @Func:   fromString
     * @Brief:  字符串转类型
     * @Param:  Name    Type    IO  Brief
     *          symbol  String  IN  类型名
     * @Return: 域类型（MFieldType）
     */
    public static MFieldType fromString(final String symbol) {
        return stringToEnum.get(symbol);
    }
}
