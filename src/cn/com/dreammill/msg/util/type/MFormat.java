/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.util.type
 * @File:       MFormat.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2018-12-26  Conan       新建
 */
package cn.com.dreammill.msg.util.type;

import cn.com.dreammill.msg.exception.IllicitTypeException;
import cn.com.dreammill.msg.tool.MBytes;
import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

/**
 * @Enum:       MFormat
 * @Brief:      格式化
 */
public class MFormat {
    private static Logger log = Logger.getLogger(MFormat.class);								//日志
    /* 常量 */
    public static final Charset DEF_ENCODING = Charset.defaultCharset();                                                // 默认编码
    /* 成员初始化 */
    private MAlign align = MAlign.RIGHT;                                                                                // 对齐方式
    private char fill = ' ';                                                                                            // 填充符
    private int len = 0;                                                                                                // 长度
    private int decimal = 0;                                                                                            // 有效数字
    private MValueType type = null;                                                                                     // 类型

    /**
     * @Func:   MFormat
     * @Brief:  构造函数
     * @Param:  Name    Type    IO  Brief
     *          format  String  IN  格式化字符串
     */
    public MFormat(final String format) throws IllicitTypeException {
        int lenBegin = 0;
        int lenEnd = format.length() - 1;
        /* 格式是否正确 */
        if(format.charAt(lenBegin) == '%') {
            ++lenBegin;
        } else {
            return;
        }
        /* 类型 */
        type = MValueType.fromChar(format.charAt(lenEnd--));
        if(type == null) {
            throw new IllicitTypeException(format);
        }
        /* 对齐方式 */
        switch(format.charAt(lenBegin)) {
            case '-':
                align = MAlign.LEFT;
                ++lenBegin;
                break;
            case '+':
                align = MAlign.RIGHT;
                ++lenBegin;
                break;
            default:
                break;
        }
        /* 填充方式 */
        switch(format.charAt(lenBegin)) {
            case ' ':
            case '0':
            case 'f':
            case 'F':
            case '*':
                fill = format.charAt(lenBegin);
                ++lenBegin;
                break;
            default:
                break;
        }
        /* 长度 */
        if(lenEnd >= lenBegin) {
            String[] lens = format.substring(lenBegin, lenEnd + 1).split("\\.", 0);
            for(int i = 0; i < lens.length; i++) {
                if(lens[i].length() > 0) {
                    if(i == 0) {
                        this.len = Integer.valueOf(lens[i]);
                    } else {
                        this.decimal = Integer.valueOf(lens[i]);
                    }
                }
            }
        }
    }

    /**
     * @Func:   getAlign
     * @Brief:  获取对齐方式
     * @Return: 对齐方式（MAlign）
     */
    public MAlign getAlign() {
        return align;
    }

    /**
     * @Func:   getAlign
     * @Brief:  获取填充符
     * @Return: 填充符（char）
     */
    public char getFill() {
        return fill;
    }

    /**
     * @Func:   getAlign
     * @Brief:  获取长度
     * @Return: 长度（int）
     */
    public int getLen() {
        return len;
    }

    /**
     * @Func:   getAlign
     * @Brief:  获取有效数字
     * @Return: 有效数字（int）
     */
    public int getDecimal() {
        return decimal;
    }

    /**
     * @Func:   getAlign
     * @Brief:  获取类型
     * @Return: 类型（MValueType）
     */
    public MValueType getType() {
        return type;
    }

    /**
     * @Func:   toString
     * @Brief:  转字符串
     * @Return: 字符串结果（String）
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("数值类型:[" +  getType() + "] ");
        sb.append("对齐方式:[" +  (getAlign() == MAlign.LEFT ? "L" : "R" )+  "] ");
        sb.append("填充字符:[" +  getFill() + "] ");
        sb.append("最小长度:[" +  getLen() + "] ");
        if(getDecimal() > 0) {
            sb.append("小数长度:[" + getDecimal() + "] ");
        }

        return sb.toString();
    }

    /**
     * @Func:   trim
     * @Brief:  去填充
     * @Param:  Name    Type    IO  Brief
     *          oldVal  byte[]  IN  原字符数组
     * @Return: 新字符数组（byte[]）
     */
    public byte[] trim(byte[] oldVal) {
        byte[] newVal = null;

        int begin = 0;
        int end = oldVal.length;

        switch(getAlign()) {
            case LEFT:
                for(; begin < end && oldVal[end - 1] == getFill(); end--) {}
                break;
            case RIGHT:
                for(; begin < end && oldVal[begin] == getFill(); begin++) {}
                break;
            default:
                break;
        }

        newVal = new byte[end - begin];
        System.arraycopy(oldVal, begin, newVal, 0, end - begin);

        return newVal;

    }

    public byte[] fill(String oldVal, String encoding) throws UnsupportedEncodingException {
        byte[] newVal = null;

        if(oldVal.getBytes(encoding).length >= len) {
            newVal = oldVal.getBytes(encoding);
        } else {
            newVal = new byte[len];
            for(int i = 0; i < len; i++) {
                newVal[i] = (byte) getFill();
            }
            switch(getAlign()) {
                case LEFT:
                    System.arraycopy(oldVal.getBytes(encoding), 0, newVal, 0, oldVal.getBytes(encoding).length);
                    break;
                case RIGHT:
                    System.arraycopy(oldVal.getBytes(encoding), 0, newVal, newVal.length - oldVal.getBytes(encoding).length, oldVal.getBytes(encoding).length);
                    break;
                default:
                    break;
            }
        }

        return newVal;
    }

    public byte[] str2bytes(String oldVal) {
        return str2bytes(oldVal, DEF_ENCODING.toString());
    }

    public byte[] str2bytes(String oldVal, String encoding) {
        byte[] newVal = null;

        try {
            switch(getType()) {
                case d:
                    newVal = fill(String.format("%d", Integer.valueOf(oldVal)), encoding);
                    break;
                case f:
                    if(decimal > 0) {
                        newVal = fill(String.format("%." + decimal + "f", Float.valueOf(oldVal)), encoding);
                    } else {
                        newVal = fill(String.format("%f", Float.valueOf(oldVal)), encoding);
                    }
                    break;
                case s:
                    newVal = fill(oldVal, encoding);
                    break;
                case x:
                    newVal = fill(Integer.toHexString(Integer.valueOf(oldVal)).toLowerCase(), encoding);
                    break;
                case X:
                    newVal = fill(Integer.toHexString(Integer.valueOf(oldVal)).toUpperCase(), encoding);
                    break;
                case b:
                case B:
                    newVal = MBytes.str2Bcd(oldVal.toUpperCase(), align);
                    break;
                default:
                    log.warn("未知类型！" + toString());
                    break;
            }
        } catch(Exception e) {
            log.warn("格式转换异常！" + toString());
            e.printStackTrace();
        }

        return newVal;
    }

    public String bytes2str(byte[] oldVal) {
        return bytes2str(oldVal, DEF_ENCODING.toString());
    }

    public String bytes2str(byte[] oldVal, String encoding) {
        String newVal = null;

        try {
            switch(getType()) {
                case d:
                    oldVal = trim(oldVal);
                    newVal = String.valueOf(Integer.parseInt(new String(oldVal)));
                    break;
                case f:
                    oldVal = trim(oldVal);
                    newVal = String.valueOf(Float.parseFloat(new String(oldVal)));
                    break;
                case s:
                    oldVal = trim(oldVal);
                    newVal = new String(oldVal, encoding);
                    break;
                case x:
                case X:
                    oldVal = trim(oldVal);
                    newVal = String.valueOf(Integer.parseInt(new String(oldVal), 16));
                    break;
                case b:
                    newVal = MBytes.bcd2str(oldVal, (len == 0 || len > oldVal.length ? oldVal.length: len)).toLowerCase();
                    break;
                case B:
                    newVal = MBytes.bcd2str(oldVal, (len == 0 || len > oldVal.length ? oldVal.length: len)).toUpperCase();
                    break;
                default:
                    log.warn("未知类型！" + toString());
                    break;
            }
        } catch(Exception e) {
            log.warn("格式转换异常！" + toString());
            e.printStackTrace();
        }

        return newVal;
    }
}
