/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.util
 * @File:       JSONUtil.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2019-01-07  Conan       新建
 */
package cn.com.dreammill.msg.util;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.util.*;

/**
 * @Enum:       JSONUtil
 * @Brief;      JSON工具包
 */
public class JSONUtil {
    /**
     * @Func:   pareseJson2Map
     * @Brief:  解析JSON到MAP
     * @Param:  Name    Type        IO  Brief
     *          jsonStr String      IN  JSON字符串
     * @Return: 解析后的MAP(Map)
     */
    public static Map<String, Object> pareseJson2Map(String jsonStr) {
        Map<String, Object> map = new HashMap<String, Object>();

        JSONObject json = JSONObject.fromObject(jsonStr);
        for(Object k : json.keySet()) {
            Object v = json.get(k);

            if(v instanceof JSONObject) {
                map.put(k.toString(), pareseJson2Map(v.toString()));
            } else if(v instanceof JSONArray) {
                List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

                Iterator<JSONObject> it = ((JSONArray)v).iterator();
                while(it.hasNext()) {
                    JSONObject json2 = it.next();
                    list.add(pareseJson2Map(json2.toString()));
                }

                map.put(k.toString(), list);
            } else {
                map.put(k.toString(), v);
            }
        }

        return map;
    }

    /**
     * @Func	pareseMap2Json
     * @Brief	解析MAP到JSON
     * @Param	@Type	@IO	@Brief
     * map		Map		IN	报文
     */
    /**
     * @Func:   pareseMap2Json
     * @Brief:  解析MAP到JSON
     * @Param:  Name    Type    IO  Brief
     *          map     Map     IN  键值对
     * @Return: JSON字符串(String)
     */
    public static String pareseMap2Json(Map<String, Object> map) {
        return JSONObject.fromObject(map).toString();
    }
}

