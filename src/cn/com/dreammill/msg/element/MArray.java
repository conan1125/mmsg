/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.element
 * @File:       MArray.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2019-01-03  Conan       新建
 */
package cn.com.dreammill.msg.element;

import org.apache.log4j.Logger;

import java.util.ArrayList;

/**
 * @Enum:       MArray
 * @Brief:      拆组包数组
 * @Extend:     ArrayList
 * @Impl:       MElement
 */
public final class MArray extends ArrayList<MElement> implements MElement {
    private static Logger log = Logger.getLogger(MArray.class);								                        // 日志

    /**
     * @Func:   addAll
     * @Brief:  添加所有
     * @Param:  Name        Type    IO  Brief
     *          oldMarray   MArray  IN  待添加数组
     * @Return: 添加是否成功(boolean)
     */
    public boolean addAll(MArray oldMarray) {
        if( null == oldMarray )
            return false;
        int i;
        int row = oldMarray.size();
        int col = -1;
        for( i = 0 ; i < row ; i++ ) {
            MElement value=oldMarray.get(i);
            log.trace("["+i+"]debug:"+value.toString());
            if( value instanceof MArray )
            {
                log.trace("["+i+"]debug:数组");
                MArray newarr=new MArray();
                newarr.addAll((MArray)value);
                this.add(newarr);
            }else if( value instanceof MDict )
            {
                MDict olddict=((MDict)value);
                log.trace("["+i+"]debug:字典"+value.toString());
                MDict newDict = new MDict();
                newDict.putAll(olddict);
                this.add(newDict);
            }else if( value instanceof MValue)
            {

                log.trace("["+i+"]debug:value:["+col+"]"+value.toString());
                this.add((MValue)value);
            }else
            {
                return false;
            }
        }


        return true;
    }

    /**
     * @Func:   put
     * @Brief:  赋值
     * @Param:  Name    Type        IO  Brief
     *          key     String      IN  字典名
     *          value   MElement    IN  值
     * @Return: 新的元素(boolean)
     */
    public MElement put(final String key, final MElement value) {
        for(MElement obj : this) {
            MElement next = null;

            if(obj instanceof MDict) {
                /* 当前KEY存放的是字典 */
                next = (MDict) obj;
                ((MDict)next).put(key, value);
            } else if(obj instanceof MArray) {
                /* 当前KEY存放的是数组 */
                next = (MArray) obj;
                ((MArray)next).put(key, value);
            } else {
                /* 当前KEY存放的不是字典 */
                next = new MDict();
                ((MDict)next).put(key, value);
            }
        }
        return this.get(0);
    }

    /**
     * @Func:   get
     * @Brief:  获取
     * @Param:  Name    Type        IO  Brief
     *          key     String      IN  字典名
     * @Return: 元素(boolean)
     */
    public MElement get(String key) {
        MArray newArray = new MArray();
        for(MElement next : this) {
            if(next instanceof MDict) {
                /* 当前KEY存放的是字典 */
                next = (MDict) next;
                newArray.add(((MDict)next).get(key));
            } else if(next instanceof MArray) {
                /* 当前KEY存放的是数组 */
                next = (MArray) next;
                ((MArray)next).get(key);
            } else {
                /* 当前KEY存放的不是字典 */
                next = new MDict();
                ((MDict)next).get(key);
            }
        }

        return newArray;
    }
}
