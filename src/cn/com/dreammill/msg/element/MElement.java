/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.element
 * @File:       MElement.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2019-01-02  Conan       新建
 */
package cn.com.dreammill.msg.element;

/**
 * @Interface:  MElement
 * @Brief:      拆组包元素
 */
public interface MElement {
}
