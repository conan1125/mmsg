/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.element
 * @File:       MValue.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2019-01-02  Conan       新建
 */
package cn.com.dreammill.msg.element;

import cn.com.dreammill.msg.config.MConfig;
import cn.com.dreammill.msg.exception.IllicitTypeException;
import cn.com.dreammill.msg.exception.IllicitValueException;
import cn.com.dreammill.msg.tool.MBytes;
import cn.com.dreammill.msg.tool.MLog;
import cn.com.dreammill.msg.tool.MStringUtils;
import cn.com.dreammill.msg.util.type.MFormat;
import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;

/**
 * @Enum:       MValue
 * @Brief:      拆组包值
 * @Impl:       MElement
 */
public final class MValue implements MElement {
    private static Logger log = Logger.getLogger(MElement.class);								//日志

    private static final String FORMAT = "%s";
    private static final String ENCODING = "UTF-8";
    private static final boolean MASK = false;
    private String value;																			//值
    private MFormat format;																			//格式
    private boolean isMask = false;
    private String encoding;


    /**
     * @Func:   getValue
     * @Brief:  返回值
     * @Return: 值(String)
     */
    public String getValue() {
        return value;
    }

    /**
     * @Func:   setValue
     * @Brief:  设置值
     * @Param:  Name        Type    IO  Brief
     *          value       String  IN  值
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @Func:   getFormat
     * @Brief:  返回格式
     * @Return: 格式(MFormat)
     */
    public MFormat getFormat() {
        return format;
    }

    /**
     * @Func:   setFormat
     * @Brief:  设置格式
     * @Param:  Name        Type    IO  Brief
     *          format      MFormat IN  格式
     */
    public void setFormat(MFormat format) {
        this.format = format;
    }

    /**
     * @Func:   isMask
     * @Brief:  是否敏感域
     * @Return: 格式(boolean)
     */
    public boolean isMask() {
        return isMask;
    }

    /**
     * @Func:   setMask
     * @Brief:  设置是否敏感域
     * @Param:  Name        Type    IO  Brief
     *          isMask      boolean IN  是否敏感域
     */
    public void setMask(boolean isMask) {
        this.isMask = isMask;
    }

    /**
     * @Func:   getEncoding
     * @Brief:  返回编码
     * @Return: 编码(String)
     */
    public String getEncoding() {
        return encoding;
    }

    /**
     * @Func:   setEncoding
     * @Brief:  设置编码
     * @Param:  Name        Type    IO  Brief
     *          encoding    String  IN  编码
     */
    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    /**
     * @Func:   MValue
     * @Brief:  构造函数
     */
    public MValue()
            throws IllicitTypeException, IllicitValueException {
        set(null, FORMAT, ENCODING, MASK);
    }

    /**
     * @Func:   MValue
     * @Brief:  构造函数
     * @Param:  Name        Type    IO  Brief
     *          value       String  IN  值
     */
    public MValue(final String value)
            throws IllicitTypeException, IllicitValueException {
        try {
            set(value.getBytes(ENCODING), FORMAT, ENCODING, MASK);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /**
     * @Func:   MValue
     * @Brief:  构造函数
     * @Param:  Name        Type    IO  Brief
     *          value       String  IN  值
     *          mask        boolean IN  是否敏感信息
     */
    public MValue(final String value , final boolean mask)
            throws IllicitTypeException, IllicitValueException {
        try {
            set(value.getBytes(ENCODING), FORMAT, ENCODING, mask);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /**
     * @Func:   MValue
     * @Brief:  构造函数
     * @Param:  Name        Type    IO  Brief
     *          value       String  IN  值
     *          format      String  IN  格式
     */
    public MValue(final String value, final String format)
            throws IllicitTypeException, IllicitValueException {
        try {
            set(value.getBytes(ENCODING), format, ENCODING, MASK);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /**
     * @Func:   MValue
     * @Brief:  构造函数
     * @Param:  Name        Type    IO  Brief
     *          value       String  IN  值
     *          format      String  IN  格式
     *          encoding    String  IN  编码
     */
    public MValue(final String value, final String format, final String encoding)
            throws IllicitTypeException, IllicitValueException {
        try {
            set(value.getBytes(encoding), format, encoding, MASK);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /**
     * @Func:   MValue
     * @Brief:  构造函数
     * @Param:  Name        Type    IO  Brief
     *          value       String  IN  值
     *          format      String  IN  格式
     *          encoding    String  IN  编码
     *          mask        boolean IN  是否敏感信息
     */
    public MValue(final String value, final String format, final String encoding, final boolean mask)
            throws IllicitTypeException, IllicitValueException {
        try {
            set(value.getBytes(encoding), format, encoding, mask);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /**
     * @Func:   MValue
     * @Brief:  构造函数
     * @Param:  Name        Type    IO  Brief
     *          value       String  IN  值
     *          format      MFormat IN  格式
     *          encoding    String  IN  编码
     *          mask        boolean IN  是否敏感信息
     */
    public MValue(final String value, final MFormat format, final String encoding, final boolean mask)
            throws IllicitTypeException, IllicitValueException {
        set(value, format, encoding, mask);
    }

    /**
     * @Func:   MValue
     * @Brief:  构造函数
     * @Param:  Name        Type    IO  Brief
     *          value       byte[]  IN  值
     */
    public MValue(final byte[] value)
            throws IllicitTypeException, IllicitValueException {
        set(value, FORMAT, ENCODING, MASK);
    }

    /**
     * @Func:   MValue
     * @Brief:  构造函数
     * @Param:  Name        Type    IO  Brief
     *          value       byte[]  IN  值
     *          format      String  IN  格式
     */
    public MValue(final byte[] value, final String format)
            throws IllicitTypeException, IllicitValueException {
        set(value, format, ENCODING, MASK);
    }

    /**
     * @Func:   MValue
     * @Brief:  构造函数
     * @Param:  Name        Type    IO  Brief
     *          value       byte[]  IN  值
     *          format      String  IN  格式
     *          encoding    String  IN  编码
     */
    public MValue(final byte[] value, final String format, final String encoding)
            throws IllicitTypeException, IllicitValueException {
        set(value, format, encoding, MASK);
    }

    /**
     * @Func:   MValue
     * @Brief:  构造函数
     * @Param:  Name        Type    IO  Brief
     *          value       byte[]  IN  值
     *          format      String  IN  格式
     *          encoding    String  IN  编码
     *          mask        boolean IN  是否敏感信息
     */
    public MValue(final byte[] value, final String format, final String encoding, final boolean mask)
            throws IllicitTypeException, IllicitValueException {
        set(value, format, encoding, mask);
    }

    /**
     * @Func:   set
     * @Brief:  赋值，初始化
     * @Param:  Name        Type    IO  Brief
     *          value       String  IN  值
     *          format      MFormat IN  格式
     *          encoding    String  IN  编码
     *          mask        boolean IN  是否敏感信息
     */
    private void set(final String value, final MFormat fmt, final String encoding, final boolean mask)
            throws IllicitTypeException, IllicitValueException {
        this.isMask = mask;
        /* 初始化 */
        this.format = fmt;
        this.value = value;
        setEncoding(encoding);
        log.trace(MBytes.formatHex(this.value.getBytes(), !MConfig.isSIE()) + " --("  + this.format.toString() + ")--> " + value);
    }

    /**
     * @Func:   set
     * @Brief:  赋值，初始化
     * @Param:  Name        Type    IO  Brief
     *          value       byte[]  IN  值
     *          format      String  IN  格式
     *          encoding    String  IN  编码
     *          mask        boolean IN  是否敏感信息
     */
    private void set(final byte[] val, final String fmt, final String encoding, final boolean mask)
            throws IllicitTypeException, IllicitValueException {
        isMask = mask;
        /* 初始化 */
        format = new MFormat(fmt);
        value = format.bytes2str(val, encoding);

        log.trace(MBytes.formatHex(this.getBytes(encoding), !MConfig.isSIE()) + " --("  + format.toString() + ")--> " + value);
    }

    /**
     * @Func:   clean
     * @Brief:  清空
     */
    public void clean() {
        this.value = null;
        this.format = null;
    }

    /**
     * @Func:   isExist
     * @Brief:  是否存在
     * @Return: 是否存在(boolean)
     */
    public boolean isExist() {
        return (value == null ? false : true);
    }

    /**
     * @Func:   getString
     * @Brief:  获取值
     * @Return: 值(String)
     */
    public String getString() {
        return value;
    }

    /**
     * @Func:   getByte
     * @Brief:  获取值
     * @Return: 值(byte)
     */
    public byte getByte() {
        return Byte.valueOf(value);
    }

    /**
     * @Func:   getBytes
     * @Brief:  获取值
     * @Return: 值(byte[])
     */
    public byte[] getBytes() {
        return value.getBytes();
    }

    /**
     * @Func:   getBytes
     * @Brief:  获取值
     * @Param:  Name        Type    IO  Brief
     *          encoding    String  IN  编码
     * @Return: 值(byte[])
     */
    public byte[] getBytes(String encoding) {
        try {
            return value.getBytes(encoding);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @Func:   getBytes
     * @Brief:  获取值
     * @Param:  Name        Type    IO  Brief
     *          fmt         String  IN  格式
     *          encoding    String  IN  编码
     * @Return: 值(byte[])
     */
    public byte[] getBytes(String fmt, String encoding) {
        try {
            format = new MFormat(fmt);
            return format.str2bytes(value, encoding);
        } catch (IllicitTypeException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @Func:   getShort
     * @Brief:  获取值
     * @Return: 值(short)
     */
    public short getShort() {
        return Short.valueOf(value);
    }

    /**
     * @Func:   getInt
     * @Brief:  获取值
     * @Return: 值(int)
     */
    public int getInt() {
        return Integer.valueOf(value);
    }

    /**
     * @Func:   getLong
     * @Brief:  获取值
     * @Return: 值(long)
     */
    public long getLong() {
        return Long.valueOf(value);
    }

    /**
     * @Func:   getDouble
     * @Brief:  获取值
     * @Return: 值(double)
     */
    public double getDouble() {
        return Double.valueOf(value);
    }

    /**
     * @Func:   getFloat
     * @Brief:  获取值
     * @Return: 值(float)
     */
    public float getFloat() {
        return Float.valueOf(value);
    }

    /**
     * @Func:   getChar
     * @Brief:  获取值
     * @Return: 值(char)
     */
    public char getChar() {
        return value.toCharArray()[0];
    }

    /**
     * @Func:   getChars
     * @Brief:  获取值
     * @Return: 值(char[])
     */
    public char[] getChars() {
        return value.toCharArray();
    }

    /**
     * @Func:   getBoolean
     * @Brief:  获取值
     * @Return: 值(boolean)
     */
    public boolean getBoolean() {
        return Boolean.valueOf(value);
    }

    /**
     * @Func:   get
     * @Brief:  获取值
     * @Param:  Name        Type    IO  Brief
     *          type        String  IN  格类型
     * @Return: 值(Object)
     */
    public Object get(final String type) {
        return MStringUtils.toType(type, value);
    }

    /**
     * @Func:   toString
     * @Brief:  获取字符串信息
     * @Return: 信息(String)
     */
    public String toString() {
        return (isMask && MConfig.isSIE() ? MLog.dataMask(value) : value);
    }

    /**
     * @Func:   toDetailed
     * @Brief:  获取字符串明细信息
     * @Return: 明细信息(String)
     */
    public String toDetailed() {
        return String.format(
                "[%15s: %s]",
                format,
                toString()
        );
    }
}