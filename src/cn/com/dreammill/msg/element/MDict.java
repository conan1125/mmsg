/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg.element
 * @File:       MDict.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2019-01-03  Conan       新建
 */
package cn.com.dreammill.msg.element;

import cn.com.dreammill.msg.MMsg;
import org.apache.log4j.Logger;

import java.util.HashMap;

/**
 * @Enum:       MDict
 * @Brief:      拆组包字典
 * @Extend:     HashMap
 * @Impl:       MElement
 */
public final class MDict extends HashMap<String, MElement> implements MElement {
    private static Logger log = Logger.getLogger(MDict.class);

    /**
     * @Func:   copyDictByName
     * @Brief:  拷贝字典根据名称
     * @Param:  Name        Type    IO  Brief
     *          inkey       String  IN  入字典名
     *          outKey      String  IO  出字典名
     *          inDict      MDict   IN  入字典
     * @Return: 是否拷贝成功(boolean)
     */
    public boolean copyDictByName(final String inkey, String outKey, final MDict inDict) {
        /* 入字典不可为空 */
        if(null == inDict) {
            return false;
        }
        String tmpKey = inkey;
        if(inkey.indexOf(MMsg.DICT_SPLIT) >= 0) {
            tmpKey = inkey.split(MMsg.DICT_SPLIT, -1)[inkey.split(MMsg.DICT_SPLIT, -1).length-1];
        }
        if(null == outKey) {
            outKey = tmpKey;
        }

        /* 判断拷贝字典的类型
         *  如果是字典、数组，则新建并赋值
         *  如果是值，则直接赋值
         */
        MElement value = this.get(inkey);
        if(value instanceof MDict) {
            log.info("字典" + ((MDict) value));
            MDict newDict=new MDict();
            newDict.putAll((MDict) value);
            inDict.put(outKey, newDict);
        } else if(value instanceof MArray) {
            MArray NewArray = new MArray();
            NewArray.addAll((MArray) value);
            inDict.put(outKey , NewArray);
        } else if(value instanceof MValue) {
            inDict.put(outKey,(MValue)value );
        } else {
            return false;
        }

        return true;
    }

    /**
     * @Func:   put
     * @Brief:  放置值
     * @Param:  Name        Type        IO  Brief
     *          key         String      IN  字典名
     *          value       MElement    IN  字典值
     * @Return: 新的元素(MElement)
     */
    public MElement put(final String key, final MElement value) {
        if(key.indexOf(MMsg.DICT_SPLIT) >= 0) {
            /* 含有下级标签 */
            MElement next = null;
            if(super.containsKey(key.substring(0, key.indexOf(MMsg.DICT_SPLIT)))) {
                /* 当前KEY存在 */
                MElement obj = this.get(key.substring(0, key.indexOf(MMsg.DICT_SPLIT)));
                if(obj instanceof MDict) {
                    /* 当前KEY存放的是字典 */
                    next = (MDict) obj;
                    ((MDict)next).put(key.substring(key.indexOf(MMsg.DICT_SPLIT) + 1), value);
                } else if(obj instanceof MArray) {
                    /* 当前KEY存放的是数组 */
                    next = (MArray) obj;
                    ((MArray)next).put(key.substring(key.indexOf(MMsg.DICT_SPLIT) + 1), value);
                } else {
                    /* 当前KEY存放的不是字典 */
                    next = new MDict();
                    ((MDict)next).put(key.substring(key.indexOf(MMsg.DICT_SPLIT) + 1), value);
                }
            } else {
                /* 但前KEY不存在 */
                next = new MDict();
                ((MDict)next).put(key.substring(key.indexOf(MMsg.DICT_SPLIT) + 1), value);
            }

            return super.put(key.substring(0, key.indexOf(MMsg.DICT_SPLIT)), next);
        } else {
            /* 不含有下级标签 */
            if(value instanceof MValue) {
                log.info("<" + key + "> <-- [" + value.toString() + "]");
            } else {
                log.debug("<" + key + "> <-- [" + value.toString() + "]");
            }
            return super.put(key, value);
        }
    }

    /**
     * @Func:   putAll
     * @Brief:  放置全部的值
     * @Param:  Name        Type    IO  Brief
     *          oldDict     MDict   IN  待拷贝的字典
     * @Return: 新的元素(MElement)
     */
    public void putAll(final MDict oldDict) {
        /* 遍历传入的所有字典 */
        for(Entry<String, MElement> entry : oldDict.entrySet())
        {
            String key = entry.getKey();
            MElement elt = entry.getValue();
            log.trace("现有字典中存在[" + key + "]");
            if(elt instanceof MDict)
            {
                log.trace("元素是MDict");
                MDict NewDict = new MDict();
                if( this.containsKey(key) )
                {
                    MDict nodeDict = (MDict) this.get(key);
                    NewDict.putAll(nodeDict);
                }
                NewDict.putAll((MDict) elt);
                this.put(key, NewDict);
            } else if(elt instanceof MArray)
            {
                log.trace("元素是MArray");
                MArray NewArray = new MArray();
                NewArray.addAll((MArray) elt);
                this.put(key , NewArray);
            } else
            {
                log.trace("元素是MValue");
                super.put(key, entry.getValue());
            }
        }
    }

    /**
     * @Func:   get
     * @Brief:  获取值
     * @Param:  Name    Type    IO  Brief
     *          key     String  IN  字典名
     * @Return: 子元素(MElement)
     */
    public MElement get(String key) {
        if(key.indexOf(MMsg.DICT_SPLIT) >= 0) {
            /* 含有下级标签 */
            MElement next = this.get(key.substring(0, key.indexOf(MMsg.DICT_SPLIT)));

            if(next instanceof MDict) {
                return ((MDict) next).get(key.substring(key.indexOf(MMsg.DICT_SPLIT) + 1));
            } else if(next instanceof MArray) {
                return ((MArray) next).get(key.substring(key.indexOf(MMsg.DICT_SPLIT) + 1));
            } else {
                return null;
            }
        } else {
            /* 不含有下级标签 */
            MElement elt = super.get(key);
            if(elt instanceof MValue) {
                log.info("<" + key + "> --> [" + elt + "]");
            } else {
                log.debug("<" + key + "> --> [" + elt + "]");
            }
            return elt;
        }
    }

    /**
     * @Func:   remove
     * @Brief:  移除字典
     * @Param:  Name    Type    IO  Brief
     *          key     String  IN  字典名
     * @Return: 移除后的元素(MElement)
     */
    public MElement remove(String key) {
        if(key.indexOf(MMsg.DICT_SPLIT) >= 0) {
            /* 含有下级标签 */
            MDict nextDict = (MDict) super.get(key.substring(0, key.indexOf(MMsg.DICT_SPLIT)));

            return nextDict.remove(key.substring(key.indexOf(MMsg.DICT_SPLIT) + 1));
        } else {
            /* 含有下级标签 */
            return super.remove(key);
        }
    }

    /**
     * @Func:   containsKey
     * @Brief:  判断字典是否存在
     * @Param:  Name    Type    IO  Brief
     *          key     String  IN  字典名
     * @Return: 字典是否存在(boolean)
     */
    public boolean containsKey(String key) {
        if(key.indexOf(MMsg.DICT_SPLIT) >= 0) {
            /* 含有下级标签 */
            MDict nextDict = (MDict) super.get(key.substring(0, key.indexOf(MMsg.DICT_SPLIT)));

            if(nextDict == null) {
                return false;
            } else {
                return nextDict.containsKey(key.substring(key.indexOf(MMsg.DICT_SPLIT) + 1));
            }
        } else {
            /* 含有下级标签 */
            return super.containsKey(key);
        }
    }
}
