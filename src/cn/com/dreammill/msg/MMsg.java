/**
 * @Project:    MMsg
 * @Package:    cn.com.dreammill.msg
 * @File:       MMsg.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2019-01-03  Conan       新建
 */
package cn.com.dreammill.msg;

import cn.com.dreammill.msg.element.MDict;
import cn.com.dreammill.msg.element.MValue;
import cn.com.dreammill.msg.exception.IllicitFormatException;
import cn.com.dreammill.msg.exception.IllicitTypeException;
import cn.com.dreammill.msg.exception.IllicitValueException;
import cn.com.dreammill.msg.exception.LackDomainException;
import cn.com.dreammill.msg.lang.MBase;
import cn.com.dreammill.msg.tool.MCfgFile;
import cn.com.dreammill.msg.tool.MLog;
import cn.com.dreammill.msg.util.MMsgInfo;
import org.apache.log4j.Logger;
import org.dom4j.DocumentException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @Class:      MMsg
 * @Brief;      拆组包
 */
public final class MMsg {
    private static Logger log = Logger.getLogger(MMsg.class);

    public static final String DICT_SPLIT = "/";
    /* 常量 */
    public static final String IDENTIFY = "Identify";
    public static final String PACK = "Pack";
    public static final String UNPACK = "Unpack";
    public static final String MAPPED = "Mapped";
    public static final String CHECK = "Check";
    public static final String DEFAULT_VERSION = "1.0";
    public static final String DEFAULT_ENCODING = System.getProperty("file.encoding");

    public static MMsgInfo identify(final String cfg, final byte[] msg)
            throws ParserConfigurationException, SAXException, IOException, IllicitFormatException, IllicitValueException, IllicitTypeException, DocumentException {
        log.info("==> 进入报文识别处理模块 ==>");
        log.debug("请求报文：" + MLog.formatHex(msg));

        /* 处理配置文件 */
        log.info("配置文件：" + cfg);
        log.trace("打开配置文件...");
        MCfgFile fileCfg = new MCfgFile(cfg);
        log.trace("检查配置文件格式...");
        Element eltRoot = parseCFG(fileCfg);

        /* 获取识别交易配置 */
        Element eltIden = null;
        if(eltRoot.getElementsByTagName(IDENTIFY).getLength() == 1) {
            eltIden = (Element)eltRoot.getElementsByTagName(IDENTIFY).item(0);
            MBase base = new MBase(eltIden);
            log.info("发现可以处理的" + base);
            log.info("进行报文识别...");
            return base.identify(msg);
        } else {
            throw new IllicitFormatException(IDENTIFY);
        }
    }

    /**
     * @Class	unpack
     * @Brief	拆报文
     * @Param	@Type	@IO	@Brief
     * cfg		String	IN	配置文件
     * msg		byte[]	IN	报文
     * @Return	字典
     */
    public static MDict unpack(final String cfg, final byte[] msg)
            throws ParserConfigurationException, SAXException, IOException, IllicitFormatException, IllicitTypeException, IllicitValueException, LackDomainException, DocumentException {
        log.info("==> 进入报文拆包处理模块 ==>");
        log.debug("报文：" + MLog.formatHex(msg));

        /* 处理配置文件 */
        log.info("配置文件：" + cfg);
        log.trace("打开配置文件...");
        MCfgFile fileCfg = new MCfgFile(cfg);
        log.trace("检查配置文件格式...");
        Element eltRoot = parseCFG(fileCfg);

        /* 获取拆包交易配置 */
        Element eltIden = null;
        if(eltRoot.getElementsByTagName(UNPACK).getLength() == 1) {
            eltIden = (Element)eltRoot.getElementsByTagName(UNPACK).item(0);
            MBase base = new MBase(eltIden);
            log.info("发现可以处理的" + base);
            log.info("进行报文拆包...");
            return base.unpack(msg);
        } else {
            throw new IllicitFormatException(UNPACK);
        }
    }

    /**
     * @Class	pack
     * @Brief	组报文
     * @Param	@Type	@IO	@Brief
     * cfg		String	IN	配置文件
     * dict		MDict	IN	字典
     * @Return	报文
     */
    public static byte[] pack(final String cfg, final MDict dict)
            throws ParserConfigurationException, SAXException, IOException, IllicitTypeException, IllicitFormatException, IllicitValueException, LackDomainException, DocumentException {
        log.info("==> 进入报文组包处理模块 ==>");
        log.debug("字典：" + dict);

        /* 处理配置文件 */
        log.info("配置文件：" + cfg);
        log.trace("打开配置文件...");
        MCfgFile fileCfg = new MCfgFile(cfg);
        log.trace("检查配置文件格式...");
        Element eltRoot = parseCFG(fileCfg);

        /* 获取组包交易配置 */
        Element eltIden = null;
        if(eltRoot.getElementsByTagName(PACK).getLength() == 1) {
            eltIden = (Element)eltRoot.getElementsByTagName(PACK).item(0);
            MBase base = new MBase(eltIden);
            log.info("发现可以处理的" + base);
            log.info("进行报文组包...");
            return base.pack(dict);
        } else {
            throw new IllicitFormatException(PACK);
        }
    }

    /**
     * @Class	mapped
     * @Brief	映射
     * @Param	@Type	@IO	@Brief
     * cfg		String	IN	配置文件
     * dict		MDict	IN	字典
     * @Return	新字典
     * @Throws	ParserConfigurationException, SAXException, IOException, IllicitFormatException, IllicitTypeException, IllicitValueException
     */
    public static MDict mapped(final String cfg, final MDict dict)
            throws ParserConfigurationException, SAXException, IOException, IllicitTypeException, IllicitFormatException, IllicitValueException, LackDomainException {
        log.info("==> 进入字典映射处理模块 ==>");
        log.debug("入字典：" + dict);

        /* 处理配置文件 */
        log.info("配置文件：" + cfg);
        log.trace("打开配置文件...");
        MCfgFile fileCfg = new MCfgFile(cfg);
        log.trace("检查配置文件格式...");
        Element eltRoot = parseCFG(fileCfg);

        /* 获取组包交易配置 */
        Element eltIden = null;
        if(eltRoot.getElementsByTagName(MAPPED).getLength() == 1) {
            eltIden = (Element)eltRoot.getElementsByTagName(MAPPED).item(0);
            MBase base = new MBase(eltIden);
            log.info("发现可以处理的" + base);
            log.info("进行字典映射...");
            return base.mapped(dict);
        } else {
            throw new IllicitFormatException(MAPPED);
        }
    }

    /**
     * @Class	check
     * @Brief	检查
     * @Param	@Type	@IO	@Brief
     * cfg		String	IN	配置文件
     * dict		MDict	IN	字典
     */
    public static void check(String cfg, MDict dict)
            throws ParserConfigurationException, SAXException, IOException, IllicitFormatException, LackDomainException {
        log.info("==> 进入有效性校验模块 ==>");
        log.debug("字典：" + dict);

        /* 处理配置文件 */
        log.info("配置文件：" + cfg);
        log.trace("打开配置文件...");
        MCfgFile fileCfg = new MCfgFile(cfg);
        log.trace("检查配置文件格式...");
        Element eltRoot = parseCFG(fileCfg);

        /* 获取有效性校验配置文件 */
        Element eltIden = null;
        if(eltRoot.getElementsByTagName(CHECK).getLength() == 1) {
            eltIden = (Element)eltRoot.getElementsByTagName(CHECK).item(0);
            MBase check = new MBase(eltIden);
            log.info("发现可以处理的" + check);
            log.info("进行有效性校验...");
            check.check(dict);
        } else {
            throw new IllicitFormatException(IDENTIFY);
        }
    }

    /**
     * @Func:   dictToBean
     * @Brief:  字典转Bean
     * @Param:  Name    Type    IO  Brief
     *          dict    MDict   IN  Bean
     *          cls     Class   IN  类
     * @Return: Bean(Object)
     * @Throws: IntrospectionException, IllegalAccessException, InstantiationException, InvocationTargetException, IllegalArgumentException
     */
    public static <T> T dictToBean(final MDict dict, final Class<T> cls)
            throws IntrospectionException, IllegalAccessException, InstantiationException, InvocationTargetException, IllegalArgumentException {
        //获取指定类的BeanInfo对象
        BeanInfo bInfo = Introspector.getBeanInfo(cls);
        //创建JavaBean对象
        T obj = cls.newInstance();
        //为JavaBean对象属性赋值
        PropertyDescriptor[] propertyDescriptors =  bInfo.getPropertyDescriptors();
        for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
            String propertyName = propertyDescriptor.getName();
            //字典中是否包含key
            if(dict.containsKey(propertyName)){
                MValue value = (MValue) dict.get(propertyName);
                Method setter = propertyDescriptor.getWriteMethod();
                String type = setter.getParameterTypes()[0].getName();
                switch (type) {
                    case "java.lang.Integer":
                    case "int":
                        if(value != null){
                            setter.invoke(obj, value.getInt());
                        }
                        break;
                    case "java.lang.String":
                    case "String":
                        if(value != null){
                            setter.invoke(obj, value.getString());
                        }
                        break;
                    case "java.lang.Short":
                    case "short":
                        if(value != null){
                            setter.invoke(obj, value.getShort());
                        }
                        break;
                    case "java.lang.Long":
                    case "long":
                        if(value != null){
                            setter.invoke(obj, value.getLong());
                        }
                        break;
                    case "java.lang.Double":
                    case "double":
                        if(value != null){
                            setter.invoke(obj, value.getDouble());
                        }
                        break;
                    case "java.lang.Float":
                    case "float":
                        if(value != null){
                            setter.invoke(obj, value.getFloat());
                        }
                        break;
                    case "java.lang.Character":
                    case "char":
                        if(value != null){
                            setter.invoke(obj, value.getChar());
                        }
                        break;
                    case "java.lang.Byte":
                    case "byte":
                        if(value != null){
                            setter.invoke(obj, value.getByte());
                        }
                        break;
                    case "java.lang.Boolean":
                    case "boolean":
                        if(value != null){
                            setter.invoke(obj, value.getBoolean());
                        }
                        break;
                    case "byte[]":
                        if(value != null){
                            setter.invoke(obj, value.getBytes());
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        return obj;
    }

    /**
     * @Func:   beanToDict
     * @Brief:  Bean转字典
     * @Param:  Name    Type    IO  Brief
     *          bean    Object  IN  Bean
     * @Return: 字典(MDict)
     * @Throws: IntrospectionException, IllegalAccessException, InstantiationException, InvocationTargetException, IllicitTypeException, IllicitValueException
     */
    public static MDict beanToDict(Object bean)
            throws IntrospectionException, IllegalAccessException, InstantiationException, InvocationTargetException, IllicitTypeException, IllicitValueException {
        if(bean == null) {
            log.info("Bean is null!");
        }
        MDict mDict = new MDict();
        //获取指定类的BeanInfo对象
        BeanInfo bInfo = Introspector.getBeanInfo(bean.getClass());

        //为JavaBean对象属性赋值
        PropertyDescriptor[] propertyDescriptors = bInfo.getPropertyDescriptors();
        for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
            String key = propertyDescriptor.getName();
            if(!(key).equals("class")){
                Method getter = propertyDescriptor.getReadMethod();
                Object value = getter.invoke(bean);
                if(value != null){
                    mDict.put(key, new MValue(value.toString()));
                }
            }
        }
        return mDict;
    }


    /**
     * @Func:   dictToMap
     * @Brief:  字典转NAP
     * @Param:  Name    Type    IO  Brief
     *          mDict   MDict   IN  字典
     * @Return: 转换后MAP(Map)
     * @Throws: Exception
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static Map dictToMap(final MDict mDict)
            throws Exception{
        Map map = new HashMap();
        Iterator<?> entries = mDict.entrySet().iterator();
        //进行遍历
        while(entries.hasNext()) {
            Map.Entry entry = (Map.Entry) entries.next();
            String key = (String)entry.getKey();
            String value = ((MValue)entry.getValue()).getString();
            map.put(key, value);
        }
        return map;
    }

    /**
     * @Func:   mapToDict
     * @Brief:  MAP转字典
     * @Param:  Name    Type    IO  Brief
     *          map     Map     IN  MAP
     * @Return: 转换后字典(MDict)
     * @Throws: Exception
     */
    @SuppressWarnings("rawtypes")
    public static MDict mapToDict(final Map map)
            throws Exception{
        MDict mDict = new MDict();
        Iterator<?> entries = map.entrySet().iterator();
        //进行遍历
        while (entries.hasNext()) {
            MDict.Entry entry = (MDict.Entry) entries.next();
            String  key = (String)entry.getKey();
            Object value =(Object)entry.getValue();
            mDict.put(key,  new MValue(value.toString()));
        }
        return mDict;
    }

    /**
     * @Func:   parseCFG
     * @Brief:  解析配置文件
     * @Param:  Name        Type        IO  Brief
     *          cfg         MCfgFile    IN  配置文件
     * @Throws: ParserConfigurationException, SAXException, IOException
     */
    private static Element parseCFG(final MCfgFile cfg)
            throws ParserConfigurationException, SAXException, IOException {

        /* 处理配置文件 */
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(cfg);

        return doc.getDocumentElement();
    }
}
