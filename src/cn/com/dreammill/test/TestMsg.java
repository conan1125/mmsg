/**
 * @Project:    TestMsg
 * @Package:    cn.com.dreammill.test
 * @File:       TestMsg.java
 * @Version:    No.     Date        Author      Brief
 *              1.0     2019-01-21  Conan       新建
 */
package cn.com.dreammill.test;

import cn.com.dreammill.msg.MMsg;
import cn.com.dreammill.msg.config.MConfig;
import cn.com.dreammill.msg.element.MDict;
import cn.com.dreammill.msg.tool.MLog;
import cn.com.dreammill.msg.util.MMsgInfo;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @Enum:       TestMsg
 * @Brief:      测试拆组包
 */
public class TestMsg {
    private static Logger log = Logger.getLogger(TestMsg.class);
    private static final String MMSG_CFG = "cfg/mmsg.properties";
    private static final String JSON_MSG_FILENAME = "./msg/JSON.txt";
    private static final String IDENTIFY_CFG = "identify.xml";
    private static final String PACK_CFG = "PACK";
    private static final String UNPACK_CFG = "UNPACK";
    private static final String CHECK_CFG = "CHECK";

    /**
     * @Func:   loadMsgFile
     * @Brief:  加载文件中的报文
     * @Param:  Name        Type    IO  Brief
     *          filename    String  IN  文件名
     * @Return: 报文
     */
    public static byte[] loadMsgFile(final String filename) {
        File file = new File(filename);
        Long fileLength = file.length();
        byte[] msg = new byte[fileLength.intValue()];
        try {
            FileInputStream in = new FileInputStream(file);
            in.read(msg);
            in.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        log.debug("请求报文：" + MLog.formatHex(msg));

        return msg;
    }

    /**
     * @Func:   main
     * @Brief:  测试主程序
     *          1.初始化拆组包配置
     *          2.加载报文
     *          3.测试拆包
     *          4.测试校验
     *          5.测试映射
     *          6.测试组包
     * @Param:  Name    Type        IO  Brief
     *          args    String[]    IO  参数
     */
    public static void main(String[] args) {
        try {
            /* 初始化拆组包配置 */
            MConfig.init(MMSG_CFG);
            /* 加载报文 */
            byte[] reqMsg = TestMsg.loadMsgFile(JSON_MSG_FILENAME);
            /* 测试识别 */
            MMsgInfo msgInfo = MMsg.identify(IDENTIFY_CFG, reqMsg);
            if(msgInfo == null) {
                log.error("未识别出报文！");
            }
            /* 测试拆包 */
            MDict reqDict = MMsg.unpack(msgInfo.getCfg(PACK_CFG), reqMsg);
            /* 测试校验 */
            MMsg.check(msgInfo.getCfg(CHECK_CFG), reqDict);
            /* 测试映射 */
            /* 测试组包 */
        } catch (Exception e) {
            log.error("初始化配置文件失败！");
            e.printStackTrace();
        }
    }
}
